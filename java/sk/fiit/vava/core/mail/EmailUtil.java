package sk.fiit.vava.core.mail;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.user.UserInfo;

public class EmailUtil {

	private Session session;
	private Authenticator auth;
	private Properties smtpProperties = new Properties();
	private Properties authProperties = new Properties();
	private static EmailUtil emailUtil = new EmailUtil();
	private InternetAddress mailAddres;
	
	private long newMails = 0;
	private String mailTo = "";
	private String text = "";
	private String subject = "";
	
	/**
	 * private constructor for Email "server", reads SMTP and authentification properties from properties file
	 */
	private EmailUtil() {
		FileInputStream input = null;
		FileInputStream inputAuth = null;
		try {
			input = new FileInputStream("resources/mail/SMTP.properties");
			smtpProperties.load(input);
			inputAuth = new FileInputStream("resources/mail/auth.properties");
			authProperties.load(inputAuth);
			auth = new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(authProperties.getProperty("name"), authProperties.getProperty("passwd"));
				}
			};
			session = Session.getDefaultInstance(smtpProperties, auth);
			mailAddres = new InternetAddress(UserInfo.getUser().getMail(), "NoReply Ais-Mail");
		} catch (IOException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Problems loading mail properties file:",e);
		} finally {
			try {
				input.close();
				inputAuth.close();
			} catch (IOException e) {
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Problems closing mail properties file:",e);
			}
			
		}
	}
	
	/**
	 * Email method to send new email to outside server via SMTP protocol and gmail account
	 * 
	 * @param toEmail - receiver email address
	 * @param subject - email subject
	 * @param text - email text
	 * @return boolean value about information if mail was send successfully or not. 
	 */
	public boolean sendEmail(String toEmail, String subject, String text) {
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			
			msg.setFrom(mailAddres);
			msg.setReplyTo(InternetAddress.parse(UserInfo.getUser().getMail(), false));
			
			msg.setSubject(subject, "UTF-8");
			
			msg.setText(text, "UTF-8");
			
			msg.setSentDate(new Date());
			
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			
			Transport.send(msg);
			return true;
		} catch (Exception e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Problem sending mail",e);
			return false;
		}
	}
	
	/**
	 * Singleton static call for this class object
	 * @return EmailUtil singleton object
	 */
	public static EmailUtil getEmailUtil() {
		return emailUtil;
	}
	/**
	 * Getter for email text
	 * @return text of forwarding/answering message
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Sets text and date of forwarding/answering message
	 * @param text
	 * @param date
	 */
	public void setText(String text, String date) {
		if (text.isEmpty() && date.isEmpty()) {
			this.text = "";
		} else {
			this.text = "\n\n\n\n"+date+"\n--------------------------------\n"+text;
		}
	}	
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		if (subject.isEmpty()) {
			this.subject = "";
		} else {
			this.subject = "Re: "+subject;
		}
	}
	public long getNewMails() {
		return newMails;
	}
	public void setNewMails(long newMails) {
		this.newMails = newMails;
	}
}
