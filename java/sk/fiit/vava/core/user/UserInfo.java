package sk.fiit.vava.core.user;

import sk.fiit.vava.core.databaseTableObjects.User;

public class UserInfo {
	
	private String name = "";
	private String surname = "";
	private String login = "";
	private String mail = "";
	private int ID = -1;
	
	private UserInfo() {
	}
	
	private static final UserInfo user = new UserInfo();
	
	public static UserInfo getUser() {
		return user;
	}
	
	public String getName() {
		return name;
	}
	public String getFullName() {
		return name + " " + surname;
	}
	
	
	public String getSurname() {
		return surname;
	}
	
	
	public String getLogin() {
		return login;
	}
	
	public int getUserID() {
		return ID;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setUser(User user) {
		this.ID = user.getId();
		this.login = user.getLogin();
		this.name = user.getMeno();
		this.surname = user.getPriezvisko();
		this.mail = user.getMail();
	}
}
