package sk.fiit.vava.core.dataObjects;

import java.util.Date;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.cell.PropertyValueFactory;

public class MailTable {

	private SimpleIntegerProperty id = new SimpleIntegerProperty();
	private SimpleIntegerProperty owners = new SimpleIntegerProperty();
	private SimpleStringProperty text = new SimpleStringProperty();
	private SimpleStringProperty subject = new SimpleStringProperty();
	private SimpleStringProperty sent = new SimpleStringProperty();
	private SimpleStringProperty seen = new SimpleStringProperty();
	private SimpleStringProperty from = new SimpleStringProperty();
	private SimpleStringProperty to = new SimpleStringProperty();
	private SimpleIntegerProperty size = new SimpleIntegerProperty();
	
	public int getId() {
		return id.get();
	}
	
	public int getOwners() {
		return owners.get();
	}
	
	public String getText() {
		return text.get();
	}
	
	public String getSubject() {
		return subject.get();
	}
	
	public String getSent() {
		return sent.get();
	}
	
	public String isSeen() {
		return seen.get();
	}
	
	public String getFrom() {
		return from.get();
	}
	
	public String getTo() {
		return to.get();
	}
	
	public Integer getSize() {
		return size.get();
	}
	
	public void setSeen(boolean seen) {
		if (seen){
			this.seen.set("         ○");
		} else {
			this.seen.set("          •");
		}
	}
	
	/**
	 * Table object for Mails
	 * @param id
	 * @param owners
	 * @param text
	 * @param subject
	 * @param sent
	 * @param seen
	 * @param from
	 * @param to
	 */
	public MailTable(int id, int owners, String text, String subject, Date sent, boolean seen, String from, String to) {
		this.id.set(id);
		this.owners.set(owners);
		this.subject.set(subject);
		this.text.set(text);
		this.sent.set(sent.toString());
		if (seen){
			this.seen.set("         ○");
		} else {
			this.seen.set("          •");
		}
		this.from.set(from);
		this.to.set(to);
		this.size.set(text.getBytes().length);
	}
	
	public static PropertyValueFactory<MailTable, Integer> getIdentificatorProperty() {
		return new PropertyValueFactory<MailTable, Integer>("id");
	}
	
	public static PropertyValueFactory<MailTable, Integer> getOwnersProperty() {
		return new PropertyValueFactory<MailTable, Integer>("owners");
	}
	
	public static PropertyValueFactory<MailTable, Integer> getSizeProperty() {
		return new PropertyValueFactory<MailTable, Integer>("size");
	}
	
	public static PropertyValueFactory<MailTable, String> getTextProperty() {
		return new PropertyValueFactory<MailTable, String>("text");
	}
	
	public static PropertyValueFactory<MailTable, String> getSubjectProperty() {
		return new PropertyValueFactory<MailTable, String>("subject");
	}
	
	public static PropertyValueFactory<MailTable, String> getSentProperty() {
		return new PropertyValueFactory<MailTable, String>("sent");
	}
	
	public static PropertyValueFactory<MailTable, String> getSeenProperty() {
		return new PropertyValueFactory<MailTable, String>("seen");
	}
	
	public static PropertyValueFactory<MailTable, String> getFromProperty() {
		return new PropertyValueFactory<MailTable, String>("from");
	}
	
	public static PropertyValueFactory<MailTable, String> getToProperty() {
		return new PropertyValueFactory<MailTable, String>("to");
	}
}
