package sk.fiit.vava.core.dataObjects;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.cell.PropertyValueFactory;

public class SubjectList {

	private SimpleIntegerProperty subjectID = new SimpleIntegerProperty();
	private SimpleStringProperty name = new SimpleStringProperty();
	private SimpleStringProperty text = new SimpleStringProperty();
	private SimpleIntegerProperty credits = new SimpleIntegerProperty();
	private SimpleIntegerProperty lector = new SimpleIntegerProperty();
	private SimpleStringProperty lectorName = new SimpleStringProperty();
	private SimpleIntegerProperty subscribers = new SimpleIntegerProperty();
	private SimpleIntegerProperty test = new SimpleIntegerProperty();
	private SimpleIntegerProperty exam = new SimpleIntegerProperty();
	
	public int getSubjectID() {
		return subjectID.get();
	}
	
	public String getName() {
		return name.get();
	}
	
	public String getText() {
		return text.get();
	}
	
	public int getCredits() {
		return credits.get();
	}
	
	public int getLector() {
		return lector.get();
	}
	
	public String getLectorName() {
		return lectorName.get();
	}
	public int getSubscribers() {
		return subscribers.get();
	}
	public int getTest() {
		return test.get();
	}
	public int getExam() {
		return exam.get();
	}
	
	/**
	 * Database object of Subject Table
	 * @param ID
	 * @param name
	 * @param credits
	 * @param lector
	 * @param text
	 */
	public SubjectList(int ID, String name, int credits, int lector, String text) {
		subjectID.set(ID);
		this.name.set(name);
		this.credits.set(credits);
		this.lector.set(lector);
		this.text.set(text);		
	}
	
	/**
	 * Database object of Subject Table with lector's name
	 * @param ID
	 * @param name
	 * @param credits
	 * @param lector
	 * @param lectorName
	 * @param text
	 */
	public SubjectList(int ID, String name, int credits, int lector, String lectorName, String text) {
		subjectID.set(ID);
		this.name.set(name);
		this.credits.set(credits);
		this.lector.set(lector);
		this.lectorName.set(lectorName);
		this.text.set(text);		
	}

	/**
	 * Database object for subject statistics
	 * @param ID
	 * @param name
	 * @param subscribers
	 */
	public SubjectList(int ID, String name, int subscribers) {
		subjectID.set(ID);
		this.name.set(name);
		this.subscribers.set(subscribers);
	}
	
	/**
	 * Database object for subject and points
	 * @param ID
	 * @param name
	 * @param credits
	 * @param test
	 * @param exam
	 */
	public SubjectList(int ID, String name, int credits, int test, int exam) {
		subjectID.set(ID);
		this.name.set(name);
		this.credits.set(credits);
		this.test.set(test);
		this.exam.set(exam);
	}
	
	public static PropertyValueFactory<SubjectList, Integer> getIDproperty() {
		return new PropertyValueFactory<SubjectList, Integer>("subjectID");
	}

	public static PropertyValueFactory<SubjectList, String> getNameProperty() {
		return new PropertyValueFactory<SubjectList, String>("name");
	}
	
	public static PropertyValueFactory<SubjectList, Integer> getCreditsProperty() {
		return new PropertyValueFactory<SubjectList, Integer>("credits");
	}
	
	public static PropertyValueFactory<SubjectList, String> getLectorProperty() {
		return new PropertyValueFactory<SubjectList, String>("lectorName");
	}
	
	public static PropertyValueFactory<SubjectList, String> getAboutProperty() {
		return new PropertyValueFactory<SubjectList, String>("text");
	}
	public static PropertyValueFactory<SubjectList, Integer> getSubscribersProperty() {
		return new PropertyValueFactory<SubjectList, Integer>("subscribers");
	}
	public static PropertyValueFactory<SubjectList, Integer> getExamProperty() {
		return new PropertyValueFactory<SubjectList, Integer>("exam");
	}
	public static PropertyValueFactory<SubjectList, Integer> getTestProperty() {
		return new PropertyValueFactory<SubjectList, Integer>("test");
	}
}
