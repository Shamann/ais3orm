package sk.fiit.vava.core.dataObjects;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.cell.PropertyValueFactory;

public class StudentTable {

	private SimpleIntegerProperty identificator = new SimpleIntegerProperty();
	private SimpleStringProperty name = new SimpleStringProperty();
	private SimpleStringProperty surname = new SimpleStringProperty();
	private SimpleStringProperty birthday = new SimpleStringProperty();
	private SimpleStringProperty faculty = new SimpleStringProperty();
	private SimpleStringProperty email = new SimpleStringProperty();
	private SimpleStringProperty birthNum = new SimpleStringProperty();
	private SimpleIntegerProperty period = new SimpleIntegerProperty();
	private SimpleIntegerProperty year = new SimpleIntegerProperty();
	private SimpleIntegerProperty points = new SimpleIntegerProperty();
	private SimpleIntegerProperty exam = new SimpleIntegerProperty();
	private SimpleIntegerProperty succ = new SimpleIntegerProperty();
	private SimpleStringProperty wholeName = new SimpleStringProperty(); 
	
	public int getIdentificator() {
		return identificator.get();
	}
	public String getName() {
		return name.get();
	}
	public String getSurname() {
		return surname.get();
	}
	public String getBirthday() {
		return birthday.get();
	}
	public String getFaculty() {
		return faculty.get();
	}
	public String getEmail() {
		return email.get();
	}
	public String getBirthNum() {
		return birthNum.get();
	}
	public int getPeriod() {
		return period.get();
	}
	public int getYear() {
		return year.get();
	}
	public int getPoints() {
		return points.get();
	}
	public int getExam() {
		return exam.get();
	}
	public int getSucc() {
		return succ.get();
	}
	public String getWholeName() {
		return wholeName.get();
	}
	
	/**
	 * @param id
	 * @param name
	 * @param surname
	 * @param birthNum
	 * @param birthday
	 * @param faculty
	 * @param period
	 * @param year
	 * @param email
	 */
	public StudentTable(int id, String name, String surname, String birthNum, String birthday, String faculty, int period, int year, String email) {
		this.name.set(name);
		this.surname.set(surname);
		this.birthday.set(birthday);
		this.birthNum.set(birthNum);
		this.faculty.set(faculty);
		this.period.set(period);
		this.year.set(year);
		this.email.set(email);
		this.identificator.set(id);
	}
	/**
	 * 
	 * @param id
	 * @param name
	 * @param rocnik
	 * @param semester
	 * @param zapocet
	 * @param skuska
	 * @param spravil
	 */
	public StudentTable(int id, String name, int rocnik, int semester, int zapocet, int skuska, int spravil) {
		this.wholeName.set(name);
		this.identificator.set(id);
		this.year.set(rocnik);
		this.period.set(semester);
		this.points.set(zapocet);
		this.exam.set(skuska);
		this.succ.set(spravil);
	}
	
	
	public static PropertyValueFactory<StudentTable, Integer> getIdentificatorProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("identificator");
	}
	
	public static PropertyValueFactory<StudentTable, String> getNameProperty() {
		return new PropertyValueFactory<StudentTable, String>("name");
	}
	
	public static PropertyValueFactory<StudentTable, String> getSurnameNameProperty() {
		return new PropertyValueFactory<StudentTable, String>("surname");
	}
	
	public static PropertyValueFactory<StudentTable, String> getBirthNumProperty() {
		return new PropertyValueFactory<StudentTable, String>("birthNum");
	}
	
	public static PropertyValueFactory<StudentTable, String> getBirthDayProperty() {
		return new PropertyValueFactory<StudentTable, String>("birthday");
	}
	
	public static PropertyValueFactory<StudentTable, Integer> getPeriodProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("period");
	}
	
	public static PropertyValueFactory<StudentTable, Integer> getYearProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("year");
	}
	public static PropertyValueFactory<StudentTable, Integer> getSuccProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("succ");
	}
	
	public static PropertyValueFactory<StudentTable, Integer> getExamProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("exam");
	}
	
	public static PropertyValueFactory<StudentTable, Integer> getPointsProperty() {
		return new PropertyValueFactory<StudentTable, Integer>("points");
	}
	public static PropertyValueFactory<StudentTable, String> getWholeNameProperty() {
		return new PropertyValueFactory<StudentTable, String>("wholeName");
	}
	public static PropertyValueFactory<StudentTable, String> getFacultyProperty() {
		return new PropertyValueFactory<StudentTable, String>("faculty");
	}
	public static PropertyValueFactory<StudentTable, String> getMailProperty() {
		return new PropertyValueFactory<StudentTable, String>("email");
	}
}
