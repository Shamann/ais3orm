package sk.fiit.vava.core.dataObjects;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.cell.PropertyValueFactory;

public class TeacherTable {

	private SimpleIntegerProperty identificator = new SimpleIntegerProperty();
	private SimpleStringProperty name = new SimpleStringProperty();
	private SimpleStringProperty surname = new SimpleStringProperty();
	private SimpleStringProperty email = new SimpleStringProperty();
	private SimpleStringProperty teachSince = new SimpleStringProperty();
	
	public int getIdentificator() {
		return identificator.get();
	}
	public String getName() {
		return name.get();
	}
	public String getSurname() {
		return surname.get();
	}
	public String getEmail() {
		return email.get();
	}
	public String getTeachSince() {
		return teachSince.get();
	}
	
	/**
	 * 
	 * @param id
	 * @param name
	 * @param surname
	 * @param teacherSince
	 * @param email
	 */
	public TeacherTable(int id, String name, String surname, String teacherSince, String email) {
		this.name.set(name);
		this.surname.set(surname);
		this.teachSince.set(teacherSince);
		this.email.set(email);
		this.identificator.set(id);
	}
	
	public static PropertyValueFactory<TeacherTable, Integer> getIdentificatorProperty() {
		return new PropertyValueFactory<TeacherTable, Integer>("identificator");
	}
	
	public static PropertyValueFactory<TeacherTable, String> getNameProperty() {
		return new PropertyValueFactory<TeacherTable, String>("name");
	}
	
	public static PropertyValueFactory<TeacherTable, String> getSurnameNameProperty() {
		return new PropertyValueFactory<TeacherTable, String>("surname");
	}
	
	public static PropertyValueFactory<TeacherTable, String> getTeachSinceProperty() {
		return new PropertyValueFactory<TeacherTable, String>("teachSince");
	}
	
	public static PropertyValueFactory<TeacherTable, String> getMailProperty() {
		return new PropertyValueFactory<TeacherTable, String>("email");
	}
}
