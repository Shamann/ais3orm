package sk.fiit.vava.core.application;
	
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.models.MailManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;


public class AisMainORM extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane)Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/LoginView.fxml").openStream());
			Scene scene = new Scene(root,1100,650);
			primaryStage.setScene(scene);
			primaryStage.setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			primaryStage.setResizable(false);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					DatabaseConnection.getMe().getSessionFactoryUtil().shutDown();
				}
			});
			primaryStage.show();
		} catch(Exception e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	private static MailManager mm = new MailManager();
	
	public static void main(String[] args) {
		
		Task<Void> mailTask = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				while(true) {
					if (!UserInfo.getUser().getMail().isEmpty()) {
						EmailUtil.getEmailUtil().setNewMails(mm.getNumberOfNewMails(UserInfo.getUser().getMail(),UserInfo.getUser().getUserID()));
					}
					Thread.sleep(1000);
				}
			}
		};
		mailTask.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), mailTask.getException());
			}
			
		});
		Thread th = new Thread(mailTask);
		th.setDaemon(true);
		th.start();
		
		launch(args);
	}
}
