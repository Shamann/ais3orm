package sk.fiit.vava.core.databaseTableObjects;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ADMINISTRATOR")
public class Administrator implements User {

	@Id
	@Column(name = "administratorID", nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name = "login")
	private String login;
	@Column(name = "passwd")
	private String passwd;
	@Column(name = "meno")
	private String meno;
	@Column(name = "priezvisko")
	private String priezvisko;
	@Column(name = "mail")
	private String mail;
	@OneToMany
	@JoinColumn(name = "admin_id")
	private Set<MailBox_User> mails;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getMeno() {
		return meno;
	}
	public void setMeno(String meno) {
		this.meno = meno;
	}
	public String getPriezvisko() {
		return priezvisko;
	}
	public void setPriezvisko(String priezvisko) {
		this.priezvisko = priezvisko;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Set<MailBox_User> getMails() {
		return mails;
	}
	public void setMails(Set<MailBox_User> mails) {
		this.mails = mails;
	}
}
