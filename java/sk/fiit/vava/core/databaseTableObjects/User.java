package sk.fiit.vava.core.databaseTableObjects;

import java.util.Set;

public interface User {

	public int getId();
	
	public void setId(int id);
	
	public String getLogin();
	
	public void setLogin(String login);
	
	public String getMeno();
	
	public void setMeno(String meno);
	
	public String getPriezvisko();
	
	public void setPriezvisko(String priezvisko);
	
	public String getMail();
	
	public void setMail(String mail);
	
	public Set<MailBox_User> getMails();
}
