package sk.fiit.vava.core.databaseTableObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ZAPISANY_PREDMET")
public class Zapisany_predmet {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "zapisany_predmetID", nullable=false)
	private int zapisany_predmetID;
	@Column(name = "semester")
	private int semester;
	@Column(name = "rok")
	private int rok;
	@Column(name = "body_zapocet")
	private int body_zapocet;
	@Column(name = "body_skuska")
	private int body_skuska;
	@Column(name = "spravil_predmet")
	private boolean spravil_predmet;
	@ManyToOne
	@JoinColumn(name = "studentID", nullable=false)
	private Student student;
	@ManyToOne
	@JoinColumn(name = "predmetID", nullable=false)
	private Predmet predmet;
	
	public Zapisany_predmet() {}
	
	public Zapisany_predmet(int semester, int rok, int body_zapocet, int body_skuska, boolean spravil_predmet, Student student, Predmet predmet) {
		this.semester = semester;
		this.rok = rok;
		this.body_skuska = body_skuska;
		this.body_zapocet = body_zapocet;
		this.spravil_predmet = spravil_predmet;
		this.student = student;
		this.predmet = predmet;
	}
	public int getZapisany_predmetID() {
		return zapisany_predmetID;
	}
	public void setZapisany_predmetID(int zapisany_predmetID) {
		this.zapisany_predmetID = zapisany_predmetID;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}
	public int getRok() {
		return rok;
	}
	public void setRok(int rok) {
		this.rok = rok;
	}
	public int getBody_zapocet() {
		return body_zapocet;
	}
	public void setBody_zapocet(int body_zapocet) {
		this.body_zapocet = body_zapocet;
	}
	public int getBody_skuska() {
		return body_skuska;
	}
	public void setBody_skuska(int body_skuska) {
		this.body_skuska = body_skuska;
	}
	public boolean isSpravil_predmet() {
		return spravil_predmet;
	}
	public void setSpravil_predmet(boolean spravil_predmet) {
		this.spravil_predmet = spravil_predmet;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Predmet getPredmet() {
		return predmet;
	}
	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
}
