package sk.fiit.vava.core.databaseTableObjects;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PREDMET")
public class Predmet {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "predmetID", nullable=false)
	private int predmetID;
	@Column(name = "meno")
	private String meno;
	@Column(name = "kredity")
	private int kredity;
	@Column(name = "popis")
	private String popis;
	@ManyToOne
	@JoinColumn(name = "prednasajuciID")
	private Prednasajuci prednasajuci;
	@OneToMany
	@JoinColumn(name = "predmetID")
	private Set<Zapisany_predmet> zapisany_predmet;
	
	public int getPredmetID() {
		return predmetID;
	}
	public void setPredmetID(int predmetID) {
		this.predmetID = predmetID;
	}
	public String getMeno() {
		return meno;
	}
	public void setMeno(String meno) {
		this.meno = meno;
	}
	public int getKredity() {
		return kredity;
	}
	public void setKredity(int kredity) {
		this.kredity = kredity;
	}
	public String getPopis() {
		return popis;
	}
	public void setPopis(String popis) {
		this.popis = popis;
	}
	public Prednasajuci getPrednasajuci() {
		return prednasajuci;
	}
	public void setPrednasajuci(Prednasajuci prednasajuci) {
		this.prednasajuci = prednasajuci;
	}
	public Set<Zapisany_predmet> getZapisany_predmet() {
		return zapisany_predmet;
	}
	public void setZapisany_predmet(Set<Zapisany_predmet> zapisany_predmet) {
		this.zapisany_predmet = zapisany_predmet;
	}
}
