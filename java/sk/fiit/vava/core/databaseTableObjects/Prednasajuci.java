package sk.fiit.vava.core.databaseTableObjects;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PREDNASAJUCI")
public class Prednasajuci {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "prednasajuciID", nullable=false)
	private int prednasajuciID;
	@Column(name = "od_dna")
	private Date od_dna;
	@OneToOne
	@JoinColumn(name = "ucitelID")
	private Ucitel ucitel;
	@OneToMany
	@JoinColumn(name = "prednasajuciID")
	private Set<Predmet> predmety;
	
	
	public int getPrednasajuciID() {
		return prednasajuciID;
	}
	public void setPrednasajuciID(int prednasajuciID) {
		this.prednasajuciID = prednasajuciID;
	}
	public Date getOd_dna() {
		return od_dna;
	}
	public void setOd_dna(Date od_dna) {
		this.od_dna = od_dna;
	}
	public Ucitel getUcitel() {
		return ucitel;
	}
	public void setUcitel(Ucitel ucitel) {
		this.ucitel = ucitel;
	}
	public Set<Predmet> getPredmety() {
		return predmety;
	}
	public void setPredmety(Set<Predmet> predmety) {
		this.predmety = predmety;
	}
}
