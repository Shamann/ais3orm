package sk.fiit.vava.core.databaseTableObjects;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MAILBOX")
public class MailBox {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "mail_id", nullable=false)
	private int id;
	@Column(name = "pocet_vlastnikov")
	private int owners;
	@Column(name = "text")
	private String text;
	@Column(name = "predmet")
	private String subject;
	@Column(name = "poslany")
	private Date sent;
	@Column(name = "komu")
	private String to;
	@Column(name = "od")
	private String from;
	@Column(name = "precitana")
	private boolean seen;
	@OneToMany
	@JoinColumn(name = "mail_id")
	private Set<MailBox_User> allUsers;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOwners() {
		return owners;
	}
	public void setOwners(int owners) {
		this.owners = owners;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Date getSent() {
		return sent;
	}
	public void setSent(Date sent) {
		this.sent = sent;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	public Set<MailBox_User> getAllUsers() {
		return allUsers;
	}
	public void setAllUsers(Set<MailBox_User> allUsers) {
		this.allUsers = allUsers;
	}
	
	
}
