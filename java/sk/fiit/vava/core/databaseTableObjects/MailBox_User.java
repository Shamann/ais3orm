package sk.fiit.vava.core.databaseTableObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mailbox_user")
public class MailBox_User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "mu_id", nullable=false)
	private int id;
	@ManyToOne
	@JoinColumn(name = "mail_id")
	private MailBox mail;
	@ManyToOne
	@JoinColumn(name = "admin_id")
	private Administrator admin;
	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Ucitel teacher;
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public MailBox getMail() {
		return mail;
	}
	public void setMail(MailBox mail) {
		this.mail = mail;
	}
	public Administrator getAdmin() {
		return admin;
	}
	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}
	public Ucitel getTeacher() {
		return teacher;
	}
	public void setTeacher(Ucitel teacher) {
		this.teacher = teacher;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
}
