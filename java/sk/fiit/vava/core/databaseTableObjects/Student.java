package sk.fiit.vava.core.databaseTableObjects;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT")
public class Student implements User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "studentID", nullable=false)
	private int id;
	@Column(name = "login")
	private String login;
	@Column(name = "passwd")
	private String passwd;
	@Column(name = "meno")
	private String meno;
	@Column(name = "priezvisko")
	private String priezvisko;
	@Column(name = "rodne_c")
	private String rodne_c;
	@Column(name = "dat_nar")
	private Date dat_nar;
	@Column(name = "semester")
	private int semester;
	@Column(name = "rocnik")
	private int rocnik;
	@Column(name = "ustav")
	private String ustav;
	@Column(name = "mail")
	private String mail;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "studentID")
	private Set<Zapisany_predmet> zapisany_predmet;
	@OneToMany
	@JoinColumn(name = "student_id")
	private Set<MailBox_User> mails;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getMeno() {
		return meno;
	}
	public void setMeno(String meno) {
		this.meno = meno;
	}
	public String getPriezvisko() {
		return priezvisko;
	}
	public void setPriezvisko(String priezvisko) {
		this.priezvisko = priezvisko;
	}
	public String getRodne_c() {
		return rodne_c;
	}
	public void setRodne_c(String rodne_c) {
		this.rodne_c = rodne_c;
	}
	public Date getDat_nar() {
		return dat_nar;
	}
	public void setDat_nar(Date dat_nar) {
		this.dat_nar = dat_nar;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}
	public int getRocnik() {
		return rocnik;
	}
	public void setRocnik(int rocnik) {
		this.rocnik = rocnik;
	}
	public String getUstav() {
		return ustav;
	}
	public void setUstav(String ustav) {
		this.ustav = ustav;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Set<Zapisany_predmet> getZapisany_predmet() {
		return zapisany_predmet;
	}
	public void setZapisany_predmet(Set<Zapisany_predmet> zapisany_predmet) {
		this.zapisany_predmet = zapisany_predmet;
	}
	public Set<MailBox_User> getMails() {
		return mails;
	}
	public void setMails(Set<MailBox_User> mails) {
		this.mails = mails;
	}
}
