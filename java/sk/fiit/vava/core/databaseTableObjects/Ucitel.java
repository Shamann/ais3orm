package sk.fiit.vava.core.databaseTableObjects;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "UCITEL")
public class Ucitel implements User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ucitelID", nullable=false)
	private int id;
	@Column(name = "login")
	private String login;
	@Column(name = "passwd")
	private String passwd;
	@Column(name = "meno")
	private String meno;
	@Column(name = "priezvisko")
	private String priezvisko;
	@Column(name = "od_dna")
	private Date od_dna;
	@Column(name = "mail")
	private String mail;
	@OneToOne
	@JoinColumn(name = "ucitelID")
	private Prednasajuci prednasajuci;
	@OneToMany
	@JoinColumn(name = "teacher_id")
	private Set<MailBox_User> mails;
	
	public Ucitel() {}
	
	public Ucitel(String login, String passwd, String meno, String priezvisko, Date od_dna, String mail) {
		this.login = login;
		this.passwd = passwd;
		this.meno = meno;
		this.priezvisko = priezvisko;
		this.od_dna = od_dna;
		this.mail = mail;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getMeno() {
		return meno;
	}
	public void setMeno(String meno) {
		this.meno = meno;
	}
	public String getPriezvisko() {
		return priezvisko;
	}
	public void setPriezvisko(String priezvisko) {
		this.priezvisko = priezvisko;
	}
	public Date getOd_dna() {
		return od_dna;
	}
	public void setOd_dna(Date od_dna) {
		this.od_dna = od_dna;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Prednasajuci getPrednasajuci() {
		return prednasajuci;
	}
	public void setPrednasajuci(Prednasajuci prednasajuci) {
		this.prednasajuci = prednasajuci;
	}
	public Set<MailBox_User> getMails() {
		return mails;
	}
	public void setMails(Set<MailBox_User> mails) {
		this.mails = mails;
	}
}
