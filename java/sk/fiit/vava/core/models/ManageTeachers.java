package sk.fiit.vava.core.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.TeacherTable;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Predmet;
import sk.fiit.vava.core.databaseTableObjects.Prednasajuci;
import sk.fiit.vava.core.databaseTableObjects.Ucitel;
import sk.fiit.vava.core.user.UserInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ManageTeachers {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	@Deprecated
	public ObservableList<TeacherTable> loadTeachersToTableSQL() throws SQLException {
		ObservableList<TeacherTable> output = FXCollections.observableArrayList();
		
		String query = "SELECT ucitelID, meno, priezvisko, od_dna, mail FROM `ucitel`";
		ResultSet teacherResult = DatabaseConnection.getMe().makeDatabaseCall(query);
		while (teacherResult.next()) {
			output.add(new TeacherTable(teacherResult.getInt("ucitelID"),
					teacherResult.getString("meno"),
					teacherResult.getString("priezvisko"), 
					teacherResult.getString("od_dna"),
					teacherResult.getString("mail")));
		}
		return output;
	}
	
	public ObservableList<TeacherTable> loadTeachersToTable() {
		ObservableList<TeacherTable> output = FXCollections.observableArrayList();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Ucitel> teacher = session.createCriteria(Ucitel.class).list();
			
			for (Ucitel u : teacher) {
				String date = dateFormat.format(u.getOd_dna());
				output.add(new TeacherTable(u.getId(),
						u.getMeno(),
						u.getPriezvisko(), 
						date,
						u.getMail()));
			}
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "SQL Problems in: " + this.getClass().getName(), e);
			return output;
		}
		return output;
	}	

	@Deprecated
	public void deleteTeacherSQL(String name, String surname, int teacherID) throws SQLException {
		String query = "DELETE FROM `ucitel` WHERE ucitelID=" + teacherID + ";";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Deleted Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());

	}
	
	public void deleteTeacher(String name, String surname, int teacherID) {
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			Ucitel teacher = (Ucitel) session.createCriteria(Ucitel.class).add(Restrictions.eq("id", teacherID)).uniqueResult();
			session.delete(teacher);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "SQL Problems in: " + this.getClass().getName(), e);
			return;
		}
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Deleted Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());
	}
	
	@Deprecated
	public String insertTeacherSQL(String name, String surname, String teacherSince, String email) throws SQLException {
		
		String login = surname.toLowerCase();
		String passwd = Configuration.getRandomPass();
		String date = teacherSince.replace("-", "");
		date.replace(".", "");
				//TODO login sufixes
		String query = "INSERT INTO `ucitel` SET login='" + login
				+ "', passwd='" + passwd + "', meno='" + name + "', priezvisko='"
				+ surname + "', od_dna=" + date + ", mail='"
				+ email + "';";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Inserted Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());
		return passwd;
	}	
	
	public String insertTeacher(String name, String surname, String teacherSince, String email) {	
		String login = surname.toLowerCase();
		String passwd = Configuration.getRandomPass();
		String date = teacherSince.replace("-", "");
		date.replace(".", "");
		Date toDate = null;
		try {
			toDate = dateFormat.parse(date);
		} catch (ParseException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Problem parsing datum in " + this.getClass().getName() + " dateForm: " + date, e);
		}
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			Ucitel nTeacher = new Ucitel(login, passwd, name, surname, toDate, email);
			session.save(nTeacher);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
			return "";
		}
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Inserted Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());
		return passwd;
	}

	@Deprecated
	public void updateTeacherSQL(int teacherID, String name, String surname, String teacherSince, String email, String passwd) throws SQLException {

		String date = teacherSince.replace("-", "");
		date.replace(".", "");
		String query = "UPDATE `ucitel` SET ";
		if (passwd.length() >= 8) {
			query += "passwd='" + passwd + "', meno='" + name + "', priezvisko='"
					+ surname + "', od_dna=" + date + ", mail='"
					+ email + "' WHERE ucitelID=" + teacherID + ";";
		} else {
			query += "meno='" + name + "', priezvisko='"
					+ surname + "', od_dna='" + teacherSince + "', mail='"
					+ email + "' WHERE ucitelID=" + teacherID + ";";
		}
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Updated Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());
	}
	
	public boolean updateTeacher(int teacherID, String name, String surname, String teacherSince, String email, String passwd) {

		String date = teacherSince.replace("-", "");
		date.replace(".", "");
		Date toDate = null;
		try {
			toDate = dateFormat.parse(date);
		} catch (ParseException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Problem parsing datum in " + this.getClass().getName() + " dateForm: " + date, e);
		}
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			Ucitel teacher = (Ucitel) session.createCriteria(Ucitel.class).add(Restrictions.eq("id", teacherID)).uniqueResult();
			teacher.setMeno(name);
			teacher.setPriezvisko(surname);
			teacher.setMail(email);
			teacher.setOd_dna(toDate);
			if (!passwd.isEmpty()) {
				teacher.setPasswd(passwd);
			}
			session.update(teacher);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
			return false;
		}
		Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Updated Teacher "+name+" " + surname + " - By admin: " + UserInfo.getUser().getFullName());
		return true;
	}

	@Deprecated
	public void dropLectoringSQL(String predmetID) throws SQLException {
		
		String query = "UPDATE `predmet` SET `prednasajuciID` = null WHERE `predmet`.`predmetID` =" + predmetID + ";";
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher " + UserInfo.getUser().getFullName() + " dropped lectioring of subject(ID) " + predmetID);
	}
	
	public void dropLectoring(String predmetID) {
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Predmet subject = (Predmet) session.createCriteria(Predmet.class).add(Restrictions.eq("predmetID", Integer.valueOf(predmetID))).uniqueResult();
			subject.setPrednasajuci(null);
			session.update(subject);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
			return;
		}
		
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher " + UserInfo.getUser().getFullName() + " dropped lectioring of subject(ID) " + predmetID);
	}

	@Deprecated
	public void becomeLectorSQL(String predmetID, int ucitelID) throws SQLException {
		String queryS = "SELECT prednasajuciID FROM prednasajuci WHERE ucitelID =" + ucitelID + ";";
		
		Statement stmt = DatabaseConnection.getMe().getConnection().createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);

		ResultSet rsS = stmt.executeQuery(queryS);
		
		if (rsS.next()) {
			
			int prednasajuciID = rsS.getInt("prednasajuciID");
			String queryU1 = "UPDATE `predmet` SET `prednasajuciID` = '" + prednasajuciID + "' WHERE `predmetID` =" + predmetID + ";";
			stmt.executeUpdate(queryU1);
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);
			Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);
		} else {
			int day = Calendar.DAY_OF_MONTH;
			int month = Calendar.MONTH;
			int year = Calendar.YEAR;
			String date = year + "-" + month +"-"+day;
			String queryI1 = "INSERT INTO prednasajuci SET od_dna='" + date + "', ucitelID=" + ucitelID + ";";
			stmt.executeUpdate(queryI1, Statement.RETURN_GENERATED_KEYS);
			ResultSet insertResult = stmt.getGeneratedKeys();
			Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector");
			
			if (insertResult.next()) {
				int prednasajuciID = insertResult.getInt(1);
				String queryI2 = "UPDATE predmet SET prednasajuciID = '" + prednasajuciID + "' WHERE predmetID =" + predmetID + ";";
				
				stmt.executeUpdate(queryI2);
				Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);
				Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);
			}
		}
		
		DatabaseConnection.getMe().getConnection().commit();
		
	}

	public void becomeLector(int predmetID, int ucitelID) {
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.getTransaction();
		
		if (!session.getTransaction().isActive()) {
			tx = session.beginTransaction();
		}
		try {		
			Prednasajuci lector = (Prednasajuci) session.createCriteria(Prednasajuci.class).add(Restrictions.eq("ucitel.id", ucitelID)).uniqueResult();
			if (lector==null) {
				Ucitel teacher = (Ucitel) session.createCriteria(Ucitel.class).add(Restrictions.eq("id", ucitelID)).uniqueResult();
				lector = new Prednasajuci();
				Date od_dna = new Date();
				dateFormat.format(od_dna);
				lector.setOd_dna(od_dna);
				lector.setUcitel(teacher);
				lector.setPredmety(null);
				
				Integer lectorID = (Integer) session.save(lector);
				lector.setPrednasajuciID(lectorID);
				Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector");
			}

			Predmet subject = (Predmet) session.createCriteria(Predmet.class).add(Restrictions.eq("predmetID", predmetID)).uniqueResult();
			subject.setPrednasajuci(lector);
			session.update(subject);
			
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);
			Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" became lector of subject(ID) "+ predmetID);

			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
			return;
		}
		
	}
	
	@Deprecated
	public void createNewSubjectSQL(String name, String credits, String about, int userID) throws SQLException {
		String query = "INSERT INTO predmet SET meno='" + name
				+ "', kredity='" + credits 
				+ "', popis='" + about + "';";
		
		Statement stmt = DatabaseConnection.getMe().getConnection().createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);
		
		stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		ResultSet insertResult = stmt.getGeneratedKeys();
		
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" created new subject "+ name);
		
		if (insertResult.next()) {
			int subjectID = insertResult.getInt(1);
			becomeLectorSQL(subjectID + "", userID);
		}
		
	}
	
	public void createNewSubject(String name, String credits, String about, int userID){
		
		Predmet subject = new Predmet();
		subject.setMeno(name);
		subject.setKredity(Integer.valueOf(credits));
		subject.setPopis(about);
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		
		Transaction tx = session.beginTransaction();
		
		try {
			
			Integer subjectID = (Integer) session.save(subject);
			becomeLector(subjectID, userID);
			
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
			return;
		}
		
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Teacher "+UserInfo.getUser().getFullName()+" created new subject "+ name);
	}
	
}
