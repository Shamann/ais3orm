package sk.fiit.vava.core.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Student;
import sk.fiit.vava.core.databaseTableObjects.Zapisany_predmet;
import sk.fiit.vava.core.user.UserInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class StudentDetailsManager {

	/**
	 * SQL String query method
	 * @param studentID
	 * @return id, name, year, period, faculty, subjCount, subjDone, credits
	 * @throws SQLException
	 */
	@Deprecated
	public String[] getStatisticsSQL(int studentID) throws SQLException {
		String[] output = new String[8];
		output[0] = UserInfo.getUser().getUserID() + "";
		output[1] = UserInfo.getUser().getName() + " " + UserInfo.getUser().getSurname();
		
		String query = "SELECT s.semester, s.rocnik, s.ustav, COUNT(*) AS predmety FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "WHERE s.studentID = " + studentID + ";";
		
		ResultSet firstInfo = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (firstInfo.next()) {
			output[2] = firstInfo.getInt("rocnik") + "";
			output[3] = firstInfo.getInt("semester") + "";
			output[4] = firstInfo.getString("ustav");
			output[5] = firstInfo.getInt("predmety") + "";
		} else {
			output[2] = "";
			output[3] = "";
			output[4] = "";
			output[5] = "";
		}
		query = "SELECT SUM(p.kredity) AS kredity, COUNT(*) "
				+ "AS spravene FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "JOIN predmet AS p ON zp.predmetID = p.predmetID "
				+ "WHERE s.studentID = "
				+ studentID +" AND zp.spravil_predmet = 1;";
		
		firstInfo = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (firstInfo.next()) {
			output[6] = firstInfo.getInt("spravene") + "";
			output[7] = firstInfo.getInt("kredity") + "";
		} else {
			output[6] = "";
			output[7] = "";
		}
		return output;
	}
	
	/**
	 * Method using Hibernate functions
	 * @param studentID
	 * @return id, name, year, period, faculty, subjCount, subjDone, credits
	 */
	public String[] getStatistics(int studentID) {
		String[] output = new String[8];
		output[0] = UserInfo.getUser().getUserID() + "";
		output[1] = UserInfo.getUser().getName() + " " + UserInfo.getUser().getSurname();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Student student = (Student)session.createCriteria(Student.class).add(Restrictions.eq("id", studentID)).uniqueResult();
			output[2] = student.getRocnik() + "";
			output[3] = student.getSemester() + "";
			output[4] = student.getUstav();
			output[5] = student.getZapisany_predmet().size()+"";
	
			
			@SuppressWarnings("unchecked")
			List<Object[]> stats = session.createCriteria(Zapisany_predmet.class).createAlias("predmet", "p")
							.add(Restrictions.eq("student.id", studentID)).add(Restrictions.eq("spravil_predmet", true))
							.setProjection(Projections.projectionList().add(Projections.rowCount()).add(Projections.sum("p.kredity"))).list();
			
			output[6] = stats.get(0)[0]+"";
			output[7] = stats.get(0)[1]+"";
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}
		
		return output;
	}
	
	@Deprecated
	public ObservableList<String> getStudentSubjectsSQL(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity, zp.spravil_predmet FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + ";";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				String tmp2 = studentSubjects.getInt("predmetID") + " - " + studentSubjects.getString("meno") + " k: " + studentSubjects.getInt("kredity") + "   •••    "+Configuration.getCfg().resourceBundle().getString("success")+" ";

				if (studentSubjects.getInt("spravil_predmet") == 1) {
					tmp2 += "A";
				} else {
					tmp2 += "N";
				}
						
				output.add(tmp2);
			}
		}
		return output;
	}

	public ObservableList<String> getStudentSubjects(String student) {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		int ID = Integer.valueOf(tmp[0]);
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			//TODO change to fcking tableView!!!
			@SuppressWarnings("unchecked")
			List<Zapisany_predmet> subscribedSubjects = (List<Zapisany_predmet>)session.createCriteria(Zapisany_predmet.class).add(Restrictions.eq("student.id", ID)).list();
			
			for (Zapisany_predmet zp : subscribedSubjects) {
				String add = new String(""+ zp.getPredmet().getPredmetID() + " - " + zp.getPredmet().getMeno() + " k: " + zp.getPredmet().getKredity() + "   •••    "+Configuration.getCfg().resourceBundle().getString("success")+" ");
				if (zp.isSpravil_predmet()) {
					add+="A";
				} else {
					add+="N";
				}
				output.add(add);	
			}
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Hibernate Problems in: " + this.getClass().getName(), e);
		}
		return output;
	}

}
