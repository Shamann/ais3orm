package sk.fiit.vava.core.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.SubjectList;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Predmet;
import sk.fiit.vava.core.databaseTableObjects.Prednasajuci;
import sk.fiit.vava.core.databaseTableObjects.Student;
import sk.fiit.vava.core.databaseTableObjects.Zapisany_predmet;
import sk.fiit.vava.core.user.UserInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SubjectManager {

	@Deprecated
	public ObservableList<String> getSubjectStatisticsSQL() throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		
		String query = "SELECT p.predmetID, p.meno, COUNT(*) "
				+ "AS studentiPredmetu from predmet AS p "
				+ "JOIN zapisany_predmet AS zp ON zp.predmetID = p.predmetID "
				+ "GROUP BY p.predmetID "
				+ "ORDER BY p.meno;";
		
		ResultSet subjectStats = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (subjectStats.next()) {
			
			output.add(subjectStats.getInt("predmetID") + " - " + subjectStats.getString("meno") + "    •••    Sub: " + subjectStats.getInt("studentiPredmetu"));
		}
		
		return output;
	}

	public ObservableList<SubjectList> getSubjectStatistics() {
		ObservableList<SubjectList> output = FXCollections.observableArrayList();
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();

		try {
			@SuppressWarnings("unchecked")
			List<Object[]> results = session
					.createCriteria(Zapisany_predmet.class)
					.createAlias("predmet", "p")
					.setProjection(
							Projections.projectionList()
									.add(Projections.groupProperty("p.predmetID"))
									.add(Projections.property("p.meno"))
									.add(Projections.rowCount()))
					.addOrder(Order.asc("p.meno")).list();
			
			for (Object[] o : results) {
				output.add(new SubjectList((int)o[0], (String)o[1], (int)(long)o[2]));
			}
			
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}	
		return output;
	}
	
	@Deprecated
	public ObservableList<String> getStudentsSQL(String name) throws SQLException {
		ObservableList<String> output =  FXCollections.observableArrayList();
		
		if (name.length() > 0) {
			String query = "SELECT studentID, meno, priezvisko FROM `student` "
					+ "WHERE LOWER(meno) LIKE '" + name.toLowerCase() 
					+ "%' or LOWER(priezvisko) LIKE'" + name.toLowerCase() + "%' ORDER BY meno;";
			
			ResultSet foundNames = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (foundNames.next()) {
				output.add(foundNames.getInt("studentID") + " - " + foundNames.getString("meno") + " " + foundNames.getString("priezvisko"));
			}
		}
		return output;
	}
	
	public ObservableList<String> getStudents(String name) {
		ObservableList<String> output =  FXCollections.observableArrayList();
		
		if (name.length() > 0) {
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			Transaction tx = session.beginTransaction();
			
			try {
				@SuppressWarnings("unchecked")
				List<Student> students = session
						.createCriteria(Student.class)
						.add(Restrictions.or(
								Restrictions.like("meno", name+"%"),
								Restrictions.like("priezvisko", name+"%"))).list();
				
				for (Student s : students) {
					output.add(s.getId() + " - " + s.getMeno() + " " + s.getMeno());
				}
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
			}	
		}
		
		return output;
	}

	@Deprecated
	public ObservableList<String> getStudentSubjectsSQL(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity "
					+ "FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + " ORDER BY p.meno;";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				output.add(studentSubjects.getInt("predmetID") + " - " + studentSubjects.getString("meno") + " k: " + studentSubjects.getInt("kredity"));
			}
		}
		return output;
	}
	
	public ObservableList<String> getStudentSubjects(String student) {
		ObservableList<String> output = FXCollections.observableArrayList();
		if (student==null) {
			return output;
		}
		String ID = student.split(" ")[0];
		if (ID.length() > 0) {
			
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			Transaction tx = session.beginTransaction();
			
			try {
				
				@SuppressWarnings("unchecked")
				List<Zapisany_predmet> subjects = session.createCriteria(Zapisany_predmet.class).createAlias("predmet", "p").add(Restrictions.eq("student.id", Integer.valueOf(ID))).addOrder(Order.asc("p.meno")).list();
				for (Zapisany_predmet zp : subjects) {
					Predmet p = zp.getPredmet();
					output.add(p.getPredmetID() + " - " + p.getMeno() + " k: " + p.getKredity());
				}
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
			}
		}
		return output;
	}
	
	@Deprecated
	public ObservableList<String> getStudentSubjectStatsSQL(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity, zp.body_skuska, zp.body_zapocet "
					+ "FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + " ORDER BY p.meno;";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				output.add(studentSubjects.getInt("predmetID") + " - "
						+ studentSubjects.getString("meno") + " k: "
						+ studentSubjects.getInt("kredity")
						+ "   \t•••••   \tTests: " + studentSubjects.getInt("body_zapocet")
						+ "pts.   \tExam: " + studentSubjects.getInt("body_skuska") + "pts.");
			}
		}
		return output;
	}
	
	public ObservableList<SubjectList> getStudentSubjectStats(String student) {
		ObservableList<SubjectList> output = FXCollections.observableArrayList();
		String ID = student.split(" ")[0];
		if (ID.length() > 0) {
			int studentID = Integer.valueOf(ID);
			
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			Transaction tx = session.beginTransaction();
			
			try {
				
				@SuppressWarnings("unchecked")
				List<Zapisany_predmet> subjects = (List<Zapisany_predmet>) session
						.createCriteria(Zapisany_predmet.class)
						.createAlias("predmet", "p")
						.add(Restrictions.eqOrIsNull("student.id", studentID))
						.addOrder(Order.asc("p.meno")).list();
				
				for (Zapisany_predmet zp : subjects) {
					output.add(new SubjectList(zp.getPredmet().getPredmetID(),
									zp.getPredmet().getMeno(), 
									zp.getPredmet().getKredity(),
									zp.getBody_zapocet(), 
									zp.getBody_skuska()));
				}
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
			}	
			
		}
		return output;
	}

	@Deprecated
	public ObservableList<String> getAllSubjectsForStudentSQL() throws SQLException {
		
		ObservableList<String> output = FXCollections.observableArrayList();
		
			String query = "SELECT p.predmetID, p.meno, p.kredity FROM predmet AS p;";
			
			ResultSet otherSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (otherSubjects.next()) {
				output.add(otherSubjects.getInt("predmetID") + " - " + otherSubjects.getString("meno") + " k: " + otherSubjects.getInt("kredity"));
			}
		return output;
	}
	
	public ObservableList<String> getAllSubjectsForStudent() {
		ObservableList<String> output = FXCollections.observableArrayList();
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Predmet> subjects = (List<Predmet>)session.createCriteria(Predmet.class).list();
			for (Predmet p : subjects) {
				output.add(p.getPredmetID() + " - " + p.getMeno() + " k: " + p.getKredity());
			}
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}
		
		return output;
	}
	
	/**
	 * Adds new subject for student with studentId
	 * @param studentId
	 * @param subjectID
	 * @param year
	 * @param semest
	 * @throws SQLException
	 */
	@Deprecated
	public void subscribeSubjectSQL(String studentID, String subjectID, String year, String semest) throws SQLException {
		if (subjectID.length() > 0 && year.length() > 0 && semest.length() > 0) {
			String query = "INSERT INTO zapisany_predmet SET rok =" + year
					+ ", semester = " + semest + ", studentID = " + studentID
					+ ", predmetID = " + subjectID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Student " + studentID + " got subscribed subject " + subjectID);
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO,"Student " + studentID + " got subscribed subject " + subjectID);
		}
	}

	public void subscribeSubject(String studentID, String subjectID, String year, String semest) {
		if (subjectID.length() > 0 && year.length() > 0 && semest.length() > 0) {
			
			int pID = Integer.valueOf(subjectID);
			int sID = Integer.valueOf(studentID);
			int rok = Integer.valueOf(year);
			int semester = Integer.valueOf(semest);
			
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			Transaction tx = session.beginTransaction();
			
			try {
				
				Predmet p = (Predmet) session.get(Predmet.class, pID);
				Student s = (Student) session.get(Student.class, sID);
				
				Zapisany_predmet zp = new Zapisany_predmet();
				zp.setPredmet(p);
				zp.setStudent(s);
				zp.setRok(rok);
				zp.setSemester(semester);
				session.save(zp);
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
				return;
			}
			
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Student " + studentID + " got subscribed subject " + subjectID);
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO,"Student " + studentID + " got subscribed subject " + subjectID);
		}
		
	}
	
	@Deprecated
	public void unsubscribeSubjectSQL(String studentID, String subjectID) throws SQLException {
		if (subjectID.length() > 0) {
			String query = "DELETE FROM zapisany_predmet WHERE studentID = "
					+ studentID + " and predmetID = " + subjectID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Student " + studentID + " got UNsubscribed subject " + subjectID);
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO,"Student " + studentID + " got UNsubscribed subject " + subjectID);
		}
		
	}
	
	public void unsubscribeSubject(String studentID, String subjectID) {
		if (subjectID.length() > 0) {
			int sID = Integer.valueOf(studentID);
			int pID = Integer.valueOf(subjectID);
			
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			Transaction tx = session.beginTransaction();
			
			try {
				Zapisany_predmet zp = (Zapisany_predmet) session
						.createCriteria(Zapisany_predmet.class)
						.add(Restrictions.eq("predmet.predmetID", pID))
						.add(Restrictions.eq("student.id", sID)).uniqueResult();
				
				session.delete(zp);
				
				tx.commit();
			} catch (Exception e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
			}
			
	
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Student " + studentID + " got UNsubscribed subject " + subjectID);
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO,"Student " + studentID + " got UNsubscribed subject " + subjectID);
		}
	}

	@Deprecated
	public ObservableList<SubjectList> getAllSubjectsSQL() throws SQLException {
		ObservableList<SubjectList> output = FXCollections.observableArrayList();
		String query = "SELECT p.*, u.ucitelID, u.meno as ucitelmeno, u.priezvisko FROM predmet AS p "
				+ "LEFT JOIN prednasajuci AS pr ON p.prednasajuciID = pr.prednasajuciID "
				+ "LEFT JOIN ucitel AS u ON pr.ucitelID = u.ucitelID ORDER BY p.meno; ";
		
		ResultSet subjRes = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while(subjRes.next()) {
			String ucitelmeno = subjRes.getString("ucitelmeno") + " " + subjRes.getString("priezvisko");
			int ucitelID = subjRes.getInt("ucitelID");
			if (ucitelmeno.equalsIgnoreCase("null null")) {
				ucitelmeno = "";
				ucitelID = -1;
			}
			output.add(new SubjectList(subjRes.getInt("predmetID"), 
					subjRes.getString("meno"), 
					subjRes.getInt("kredity"), 
					ucitelID, 
					ucitelmeno, 
					subjRes.getString("popis")));
		}
		return output;
	}
	
	public ObservableList<SubjectList> getAllSubjects() {
		ObservableList<SubjectList> output = FXCollections.observableArrayList();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Predmet> subjects = (List<Predmet>)session.createCriteria(Predmet.class).addOrder(Order.asc("meno")).list();
			
			for (Predmet p : subjects) {
				String ucitelMeno = "";
				int prednasajuciID = -1;
				if (p.getPrednasajuci()!=null) {
					ucitelMeno = (p.getPrednasajuci().getUcitel().getMeno()+ " " + p.getPrednasajuci().getUcitel().getPriezvisko());
					prednasajuciID = p.getPrednasajuci().getUcitel().getId();
				}
				
				if (ucitelMeno.equalsIgnoreCase("null null")) {
					ucitelMeno = "";
				}
				output.add(new SubjectList(p.getPredmetID(), 
								p.getMeno(), 
								p.getKredity(), 
								prednasajuciID,
								ucitelMeno,
								p.getPopis()));
			}
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}
		
		return output;
	}

	@Deprecated
	public void updateSubjectSQL(String id, String name, String credits, String about) throws SQLException {
		String query = "UPDATE predmet SET meno='" + name + "', kredity='" + credits + "', popis='" + about + "' WHERE predmet.predmetID=" + id + ";";
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Updated Subject " + id + " - " +name + " By teacher " + UserInfo.getUser().getFullName());
	}

	public void updateSubject(String id, String name, String credits, String about) {
		int predmetID = Integer.valueOf(id);
		int kredity =  Integer.valueOf(credits);
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Predmet predmet = (Predmet) session.get(Predmet.class, predmetID);
			predmet.setMeno(name);
			predmet.setPopis(about);
			predmet.setKredity(kredity);
			session.update(predmet);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
			return;
		}
		
		Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO,"Updated Subject " + id + " - " +name + " By teacher " + UserInfo.getUser().getFullName());
	}
	
	
	@Deprecated
	public boolean hasStudentSubjectSQL(String studentID, String subjectID) throws SQLException {
		String query = "SELECT zp.zapisany_predmetID "
				+ "FROM zapisany_predmet AS zp "
				+ "JOIN student AS s ON s.studentID = zp.studentID "
				+ "WHERE s.studentID =" + studentID + " AND zp.predmetID =" + subjectID + ";";
		
		ResultSet has = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (has.next()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasStudentSubject(String studentID, String subjectID) {
		
		int sID = Integer.valueOf(studentID);
		int pID = Integer.valueOf(subjectID);
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Zapisany_predmet zp = null;
		
		try {
			zp = (Zapisany_predmet) session
					.createCriteria(Zapisany_predmet.class)
					.add(Restrictions.eq("student.id", sID))
					.add(Restrictions.eq("predmet.predmetID", pID))
					.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE,
					"Hibernate Problem at " + this.getClass().getName(), e);
			return false;
		}

		if (zp == null) {
			return false;
		} else {
			return true;
		}
	}

	@Deprecated
	public ObservableList<String> getLectorSubjectsSQL(int ucitelID) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		
		String query = "SELECT p.predmetID, p.meno "
				+ "FROM predmet AS p "
				+ "JOIN prednasajuci AS pr ON p.prednasajuciID = pr.prednasajuciID "
				+ "WHERE pr.ucitelID =" + ucitelID + " ORDER BY p.meno";
		
		ResultSet subjects = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (subjects.next()) {
			output.add(subjects.getInt("predmetID") + " - " + subjects.getString("meno"));
		}
		
		return output;
	}
	
	public ObservableList<String> getLectorSubjects(int ucitelID) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
		
			Prednasajuci lector = (Prednasajuci) session.createCriteria(Prednasajuci.class)
					.createAlias("predmety", "pr")
					.createAlias("ucitel", "u")
					.add(Restrictions.eq("u.id", ucitelID))
					.addOrder(Order.asc("pr.meno")).uniqueResult();
			
			for (Predmet predmet : lector.getPredmety()) {
				output.add(predmet.getPredmetID() + " - " + predmet.getMeno());
			}
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}
		
		return output;
	}

	public List<Predmet> getSubjects() {
		List<Predmet> output = new ArrayList<Predmet>();
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Predmet> subjects = session.createCriteria(Predmet.class).addOrder(Order.asc("meno")).list();
			output.addAll(subjects);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problem at " + this.getClass().getName(), e);
		}
		
		return output;
	}
	
}
