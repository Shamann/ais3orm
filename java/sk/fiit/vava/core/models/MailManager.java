package sk.fiit.vava.core.models;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.MailTable;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Administrator;
import sk.fiit.vava.core.databaseTableObjects.MailBox;
import sk.fiit.vava.core.databaseTableObjects.MailBox_User;
import sk.fiit.vava.core.databaseTableObjects.Student;
import sk.fiit.vava.core.databaseTableObjects.Ucitel;
import sk.fiit.vava.core.databaseTableObjects.User;
import sk.fiit.vava.core.user.UserInfo;

public class MailManager {
	
	private Comparator<? super MailTable> comp = new Comparator<MailTable>() {

		@Override
		public int compare(MailTable arg0, MailTable arg1) {
			return arg1.getSent().compareTo(arg0.getSent());
		}
	};
	
	public void sentOut(String toEmail, String subject, String text) {
		
		MailBox_User mu = new MailBox_User();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Student student = null;
		Ucitel ucitel = null;
		Administrator admin = null;
		
		try {
		
			if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
				ucitel = (Ucitel) session.createCriteria(Ucitel.class).add(Restrictions.eq("id", UserInfo.getUser().getUserID())).uniqueResult();
				mu.setTeacher(ucitel);
			} else if (UserInfo.getUser().getUserID() >= Configuration.admin) {
				admin = (Administrator) session.createCriteria(Administrator.class).add(Restrictions.eq("id", UserInfo.getUser().getUserID())).uniqueResult();
				mu.setAdmin(admin);
			} else {
				student = (Student) session.createCriteria(Student.class).add(Restrictions.eq("id", UserInfo.getUser().getUserID())).uniqueResult();
				mu.setStudent(student);
			}
			
			MailBox mail = new MailBox();
			mail.setOwners(1);
			mail.setSeen(false);
			mail.setSent(new Date());
			mail.setText(text);
			mail.setFrom(UserInfo.getUser().getMail());
			mail.setTo(toEmail);
			mail.setSubject(subject);
			
			session.save(mail);
			mu.setMail(mail);
			session.save(mu);
			
			tx.commit();
			
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
		}
		
	}

	public boolean sendEmail(String toEmail, String subject, String text) {
		
		MailBox_User muFrom = new MailBox_User();
		MailBox_User muTo = new MailBox_User();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		Student student = null;
		Ucitel ucitel = null;
		Administrator admin = null;
		Student student2 = null;
		Ucitel ucitel2 = null;
		Administrator admin2 = null;
		
		try {
			
			MailBox mail = new MailBox();
			mail.setSeen(false);
			mail.setText(text);
			mail.setSubject(subject);
			mail.setSent(new Date());
			mail.setTo(toEmail);
			mail.setFrom(UserInfo.getUser().getMail());
			mail.setOwners(2);
			
			if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
				ucitel = (Ucitel) session
						.createCriteria(Ucitel.class)
						.add(Restrictions.eq("id", UserInfo.getUser()
								.getUserID())).uniqueResult();
				muFrom.setTeacher(ucitel);
			} else if (UserInfo.getUser().getUserID() >= Configuration.admin) {
				admin = (Administrator) session
						.createCriteria(Administrator.class)
						.add(Restrictions.eq("id", UserInfo.getUser()
								.getUserID())).uniqueResult();
				muFrom.setAdmin(admin);
			} else {
				student = (Student) session
						.createCriteria(Student.class)
						.add(Restrictions.eq("id", UserInfo.getUser()
								.getUserID())).uniqueResult();
				muFrom.setStudent(student);
			}
			
			student2 = (Student) session.createCriteria(Student.class).add(Restrictions.eq("mail", toEmail)).uniqueResult();
			ucitel2 = (Ucitel) session.createCriteria(Ucitel.class).add(Restrictions.eq("mail", toEmail)).uniqueResult();
			admin2 = (Administrator) session.createCriteria(Administrator.class).add(Restrictions.eq("mail", toEmail)).uniqueResult();
			
			if (ucitel2!=null) {
				muTo.setTeacher(ucitel2);
			} else if (admin2!=null) {
				muTo.setAdmin(admin2);
			} else if (student2!=null){
				muTo.setStudent(student2);
			} else {
				tx.rollback();
				return false;
			}
			
			session.save(mail);
			
			muTo.setMail(mail);
			muFrom.setMail(mail);
			
			session.save(muFrom);
			session.save(muTo);
			
			tx.commit();			
			
			return true;
		} catch (HibernateException e) {
			if (tx!=null) {
				tx.rollback();
			}
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
			return false;
		}
	}
	
	public ObservableList<MailTable> getSentMails(int userID, String userMail) {
		ObservableList<MailTable> output = FXCollections.observableArrayList();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		User user = null;
		Set<MailBox_User> mails = null;
		
		try {
			
			if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
				user = (Ucitel) session
						.createCriteria(Ucitel.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = user.getMails();
			} else if (UserInfo.getUser().getUserID() >= Configuration.admin) {
				user = (Administrator) session
						.createCriteria(Administrator.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = user.getMails();
			} else {
				user = (Student) session
						.createCriteria(Student.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = user.getMails();
			}
			
			
			for (MailBox_User m : mails) {
				MailBox mb = m.getMail();
				if (mb.getFrom().equals(userMail)) {
					output.add(new MailTable(mb.getId(), 
											 mb.getOwners(), 
											 mb.getText(), 
											 mb.getSubject(), 
											 mb.getSent(),
											 mb.isSeen(),
											 mb.getFrom(),
											 mb.getTo()));
				}
			}
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
		}
		
		output.sort(comp);
		return output;
	}

	public ObservableList<MailTable> getReceivedMails(int userID, String userMail) {
		ObservableList<MailTable> output = FXCollections.observableArrayList();
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		Ucitel ucitel = null;
		Administrator admin = null;
		Student student = null;
		Set<MailBox_User> mails = null;
		
		try {
			
			if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
				ucitel = (Ucitel) session
						.createCriteria(Ucitel.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = ucitel.getMails();
			} else if (UserInfo.getUser().getUserID() >= Configuration.admin) {
				admin = (Administrator) session
						.createCriteria(Administrator.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = admin.getMails();
			} else {
				student = (Student) session
						.createCriteria(Student.class)
						.add(Restrictions.eq("id", userID)).uniqueResult();
				mails = student.getMails();
			}
			
			
			for (MailBox_User m : mails) {
				MailBox mb = m.getMail();
				if (mb.getTo().equals(userMail)) {
					output.add(new MailTable(mb.getId(), 
											 mb.getOwners(), 
											 mb.getText(), 
											 mb.getSubject(), 
											 mb.getSent(),
											 mb.isSeen(),
											 mb.getFrom(),
											 mb.getTo()));
				}
			}
			
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
		}
		
		output.sort(comp);
		return output;
	}

	public boolean deleteMail(int mail_id, int userID) {
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
	
		try {
			
			MailBox message = (MailBox) session.createCriteria(MailBox.class).add(Restrictions.eq("id", mail_id)).uniqueResult();
			@SuppressWarnings("unchecked")
			List<MailBox_User> userMessage = session.createCriteria(MailBox_User.class).add(Restrictions.eq("mail.id", mail_id)).list();
			
			message.setOwners(message.getOwners()-1);
			for (MailBox_User mu : userMessage) {
				if (mu.getAdmin()!=null && mu.getAdmin().getId()==userID) {
					session.delete(mu);
					break;
				}
				if (mu.getTeacher()!=null && mu.getTeacher().getId()==userID) {
					session.delete(mu);
					break;
				}
				if (mu.getStudent()!=null && mu.getStudent().getId()==userID) {
					session.delete(mu);
					break;
				}
			} 
			
			if (message.getOwners()==0) {
				session.delete(message);
			} else {
				session.update(message);
			}
			
			tx.commit();
			return true;
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
		
		return false;
	}

	public void setSeen(int mail_id) {
		Task<Void> task = new Task<Void>() {
			
			@Override
			protected Void call() throws Exception {
				Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
				Transaction tx = session.beginTransaction();
			
				try {
					
					MailBox message = (MailBox) session.createCriteria(MailBox.class).add(Restrictions.eq("id", mail_id)).uniqueResult();
					message.setSeen(true);
					
					session.update(message);
					
					tx.commit();
				} catch (HibernateException e) {
					tx.rollback();
					Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
				}
				return null;
			}
		};
		new Thread(task).start();
	}

	public Long getNumberOfNewMails(String mail, int user_id) {

		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			@SuppressWarnings("unchecked")
			List<MailBox> mails = session.createCriteria(MailBox.class).add(Restrictions.eq("to", mail)).add(Restrictions.eq("seen", false)).list();
			
			long count = 0;
			for (MailBox mb : mails) {
				for (MailBox_User mu : mb.getAllUsers()) {
					if (mu.getAdmin()!=null && mu.getAdmin().getId()==user_id) {
						count++;
						break;
					}
					if (mu.getTeacher()!=null && mu.getTeacher().getId()==user_id) {
						count++;
						break;
					}
					if (mu.getStudent()!=null && mu.getStudent().getId()==user_id) {
						count++;
						break;
					}
				}
			}
			tx.commit();
			return count;
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName(), e);
		}
		return (long) -1;
	}

}
