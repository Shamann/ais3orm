package sk.fiit.vava.core.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Administrator;
import sk.fiit.vava.core.databaseTableObjects.Student;
import sk.fiit.vava.core.databaseTableObjects.Ucitel;
import sk.fiit.vava.core.user.UserInfo;

public class Login {


	@Deprecated
	public boolean verifyLoginSQL(String login, String passwd) {
		
		if (login.isEmpty() || passwd.isEmpty()) {
			return false;
		}
				
		String query = "select studentID from student where login = '" + login + "' and passwd = '" + passwd + "' "
			   + "union select ucitelID from ucitel where login = '" + login + "' and passwd = '" + passwd + "' "
			   + "union select administratorID from administrator where login = '" + login + "' and passwd = '" + passwd + "';";
		ResultSet loginResult;
		try {
			loginResult = DatabaseConnection.getMe().makeDatabaseCall(query);
			if (loginResult.next()) {
				int ID = loginResult.getInt("studentID");
				if (ID > 0) {
					return true;
				}
			}
		} catch (SQLException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL Problems in: " + this.getClass().getName() + "with login and passwd: " + login + ", " + passwd, e);
			e.printStackTrace();
		}

		return false;
	}
	
	public boolean verifyLogin(String login, String passwd) {
		
		if (login.isEmpty() || passwd.isEmpty()) {
			return false;
		}
		
		Session session = DatabaseConnection.getMe().getSessionFactory().openSession();
		final Transaction tx = session.beginTransaction();
		
		try { 
			@SuppressWarnings("unchecked")
			List<Student> userStudents = session.createCriteria(Student.class)
					.add(Restrictions.eq("login", login))
					.add(Restrictions.eq("passwd", passwd)).list();
			for (Student s : userStudents) {
				if (s != null) {
					UserInfo.getUser().setUser(s);
					tx.commit();
					return true;
				}
			}
			@SuppressWarnings("unchecked")
			List<Administrator> userAdministrators = session
					.createCriteria(Administrator.class)
					.add(Restrictions.eq("login", login))
					.add(Restrictions.eq("passwd", passwd)).list();
			for (Administrator s : userAdministrators) {
				if (s != null) {
					UserInfo.getUser().setUser(s);
					tx.commit();
					return true;
				}
			}
			@SuppressWarnings("unchecked")
			List<Ucitel> userUcitelList = session.createCriteria(Ucitel.class)
					.add(Restrictions.eq("login", login))
					.add(Restrictions.eq("passwd", passwd)).list();
			for (Ucitel s : userUcitelList) {
				if (s != null) {
					UserInfo.getUser().setUser(s);
					tx.commit();
					return true;
				}
			}
		} catch (HibernateException e) {
			if (tx!=null) {
				tx.rollback();
			}
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL/Hibernate Problems in: " + this.getClass().getName() + "with login and passwd: " + login + ", " + passwd, e);
		} finally {
			session.close();
		}
		
		return false;
	}
}
