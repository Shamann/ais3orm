package sk.fiit.vava.core.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.StudentTable;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.databaseTableObjects.Student;
import sk.fiit.vava.core.databaseTableObjects.Zapisany_predmet;
import sk.fiit.vava.core.user.UserInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ManageStudents {
	
	private SimpleDateFormat parseFormat = new SimpleDateFormat("yyyyMMdd");
	
	/**
	 * SQL method
	 * @return
	 * @throws SQLException
	 */
	@Deprecated
	public ObservableList<StudentTable> loadStudentsToTableSQL() throws SQLException {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		
		String query = "SELECT studentID, meno, priezvisko, rodne_c, dat_nar, semester, rocnik, ustav, mail FROM `student`";
		ResultSet studentResult = DatabaseConnection.getMe().makeDatabaseCall(query);
		while (studentResult.next()) {
			output.add(new StudentTable(studentResult.getInt("studentID"),
					studentResult.getString("meno"),
					studentResult.getString("priezvisko"), 
					studentResult.getString("rodne_c"), 
					studentResult.getString("dat_nar"), 
					studentResult.getString("ustav"), 
					studentResult.getInt("semester"), 
					studentResult.getInt("rocnik"),
					studentResult.getString("mail")));
		}
		return output;
	}
	
	/**
	 * Hibernate method
	 * @return ObservableList - StudentTable
	 * 
	 */
	public ObservableList<StudentTable> loadStudentsToTable() {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		final Transaction tx = session.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Student> students = session.createCriteria(Student.class).list();
			for (Student s : students) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String date = dateFormat.format(s.getDat_nar());
				output.add(new StudentTable(s.getId(),s.getMeno(), s.getPriezvisko(), s.getRodne_c(), date, s.getUstav(), s.getSemester(), s.getRocnik(), s.getMail()));
			}
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.SEVERE, "Hibernate error", e);
		}
		return output;
	}
	
	/**
	 * SQL method
	 * @param name
	 * @param surname
	 * @param birthNum
	 * @throws SQLException
	 */
	@Deprecated
	public void deleteStudentSQL(String name, String surname, String birthNum) throws SQLException {
		String query = "DELETE FROM `student` where meno = '" + name
				+ "' and priezvisko = '" + surname + "' and rodne_c = '"
				+ birthNum + "';"; 
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Deleted Student: " + name + " " + surname + " - By admin: " + UserInfo.getUser().getFullName());
	}

	/**
	 * Hibernate method
	 * @param name
	 * @param surname
	 * @param birthNum
	 * @throws HibernateException
	 */
	public void deleteStudent(String name, String surname, String birthNum) {
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Criteria crit = session.createCriteria(Student.class);
			crit.add(Restrictions.eq("meno", name));
			crit.add(Restrictions.eq("priezvisko", surname));
			crit.add(Restrictions.eq("rodne_c", birthNum));
			Student student = (Student)crit.uniqueResult();
			session.delete(student);
			tx.commit();			
		} catch (HibernateException e) {
			tx.rollback();
			session.close();
		}
		Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Deleted Student: " + name + " " + surname + " - By admin: " + UserInfo.getUser().getFullName());
	}
	
	/**
	 * SQL method
	 * @param name
	 * @param surname
	 * @param birthnum
	 * @param birthdate
	 * @param period
	 * @param year
	 * @param faculty
	 * @param email
	 * @throws SQLException
	 */
	@Deprecated
	public void insertStudentSQL(String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email) throws SQLException {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		String login = "00" + surname.toLowerCase() + "14";
		String passwd = Configuration.getRandomPass();
		String date = birthdate.replace("-", "");
		date.replace(".", "");
		
				//TODO login sufixes
		String query = "INSERT INTO `student` SET login='" + login
				+ "', passwd='" + passwd + "', meno='" + name + "', priezvisko='"
				+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
				+ date + "', semester=" + semester + ", rocnik=" + rocnik
				+ ", ustav='" + faculty + "', mail='"
				+ email + "';";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Inserted Student: " + name + " " + surname + " - By admin: " + UserInfo.getUser().getFullName());
		
	}
	
	/**
	 * Hibernate method
	 * @param name
	 * @param surname
	 * @param birthnum
	 * @param birthdate
	 * @param period
	 * @param year
	 * @param faculty
	 * @param email
	 */
	public void insertStudent(String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email) {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		String login = "00" + surname.toLowerCase() + "14";
		String passwd = Configuration.getRandomPass();
		
		Date toDate = null;
		try {
			toDate = parseFormat.parse(birthdate);
		} catch (ParseException e) {
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.WARNING, "Wrong format of date " + this.getClass().getName(), e);
			return;
		}
		
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Student student = new Student();
			student.setDat_nar(toDate);
			student.setLogin(login);
			student.setMail(email);
			student.setMeno(name);
			student.setPasswd(passwd);
			student.setPriezvisko(surname);
			student.setRocnik(rocnik);
			student.setSemester(semester);
			student.setRodne_c(birthnum);
			student.setUstav(faculty);
			session.save(student);
			tx.commit();
			
		} catch (HibernateException e) {
			tx.rollback();
			
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.SEVERE, "Hibernate error", e);
		}
		Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Inserted Student: " + name + " " + surname + " - By admin: " + UserInfo.getUser().getFullName());
	}

	@Deprecated
	public void updateStudentSQL(int studentID, String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email, String passwd) throws SQLException {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		String date = birthdate.replace("-", "");
		date.replace(".", "");
		
		String query = "UPDATE `student` SET ";
		if (passwd.length() >= 8) {
			query += "passwd='" + passwd + "', meno='" + name + "', priezvisko='"
					+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
					+ date + "', semester=" + semester + ", rocnik=" + rocnik
					+ ", ustav='" + faculty + "', mail='"
					+ email + "' WHERE studentID=" + studentID + ";";

			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Student " + name + " " + surname + " info + pass changed - By admin: " + UserInfo.getUser().getFullName());
		} else {
			query += "meno='" + name + "', priezvisko='"
					+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
					+ date + "', semester=" + semester + ", rocnik=" + rocnik
					+ ", ustav='" + faculty + "', mail='"
					+ email + "' WHERE studentID=" + studentID + ";";

			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Student :" + name + " " + surname + " info changed - By admin: " + UserInfo.getUser().getFullName());
		}
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}
	
	public void updateStudent(int studentID, String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email, String passwd) {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		Date toDate = null;
		try {
			toDate = parseFormat.parse(birthdate);
		} catch (ParseException e) {
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.WARNING, "Wrong format of date " + this.getClass().getName(), e);
			return;
		}
		Session session = DatabaseConnection.getMe().getSessionFactory().openSession();
		final Transaction tx = session.beginTransaction();
		try {
			Student student = (Student)session.get(Student.class, new Integer(studentID));
			student.setMeno(name);
			student.setPriezvisko(surname);
			student.setDat_nar(toDate);
			student.setRodne_c(birthnum);
			student.setRocnik(rocnik);
			student.setSemester(semester);
			student.setUstav(faculty);
			student.setMail(email);
			if(!passwd.isEmpty()) {
				student.setPasswd(passwd);
			}
			session.update(student);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			session.close();
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "SQL Problems in: " + this.getClass().getName(), e);
		} finally {
			session.close();
		}
		Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Student :" + name + " " + surname + " info changed - By admin: " + UserInfo.getUser().getFullName());	
	}
	
	/**
	 * SQL method
	 * @param subjectID
	 * @return observable list of all students attending this subject
	 * @throws SQLException
	 */
	@Deprecated
	public ObservableList<StudentTable> getStudentsFromSubjectSQL(String subjectID) throws SQLException {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		String query = "SELECT s.studentID, s.meno, s.priezvisko, s.rocnik, s.semester, zp.body_zapocet, zp.body_skuska, zp.spravil_predmet "
				+ "FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "JOIN predmet AS p ON p.predmetID = zp.predmetID "
				+ "WHERE p.predmetID =" + subjectID 
				+ " GROUP BY s.studentID"
				+ " ORDER BY s.meno;";
		
		ResultSet studentFrom = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (studentFrom.next()) {
			output.add(new StudentTable(studentFrom.getInt("studentID"),
					studentFrom.getString("meno") + " " +
					studentFrom.getString("priezvisko"),
					studentFrom.getInt("rocnik"),
					studentFrom.getInt("semester"),
					studentFrom.getInt("body_zapocet"),
					studentFrom.getInt("body_skuska"),
					studentFrom.getInt("spravil_predmet")
					));
		}
		
		return output;
	}

	public ObservableList<StudentTable> getStudentsFromSubject(String subjectID) {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		int predmetID = Integer.valueOf(subjectID);
		Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
		final Transaction tx = session.beginTransaction();
		
		try {
			@SuppressWarnings("unchecked")
			List<Zapisany_predmet> subscribedSubjects = session
					.createCriteria(Zapisany_predmet.class)
					.add(Restrictions.eq("predmet.predmetID", predmetID))
					.list();
			for (Zapisany_predmet zp : subscribedSubjects) {
				int spravil = zp.isSpravil_predmet() ? 1 : 0;
				output.add(new StudentTable(zp.getStudent().getId(), zp
						.getStudent().getMeno() + " " + 
						zp.getStudent().getPriezvisko(),
						zp.getStudent().getRocnik(),
						zp.getStudent().getSemester(),
						zp.getBody_zapocet(), 
						zp.getBody_skuska(), 
						spravil));
			}
			
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			session.close();
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "SQL Problems in: " + this.getClass().getName(), e);
		}
		return output;
	}
	
	@Deprecated
	public boolean addYearPointsSQL(String oldValue, String addNewValue, String subjectID, String studentID) throws SQLException {
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			String query = "UPDATE zapisany_predmet SET body_zapocet =" + oldValue +
					" + " + addNewValue + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Student: " + studentID + " year pts changed oldV: "+ oldValue + ", newV: " + addNewValue + " - By teacher: " + UserInfo.getUser().getFullName());
			return true;
		} else {
			return false;
		}
	}
	
	public boolean addYearPoints(String oldValue, String addNewValue, String subjectID, String studentID){
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			final Transaction tx = session.beginTransaction();
			
			try {
				@SuppressWarnings("unchecked")
				List<Zapisany_predmet> predmety = session.createCriteria(Zapisany_predmet.class)
						.add(Restrictions.eq("predmet.predmetID", new Integer(subjectID)))
						.add(Restrictions.eq("student.id", new Integer(studentID)))
						.list();
				predmety.get(0).setBody_zapocet(predmety.get(0).getBody_zapocet() + Integer.valueOf(addNewValue));
				session.update(predmety.get(0));
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
				return false;
			}
			Loggers.getLoggers().STUDENT_LOGGER.log(Level.INFO, "Student: " + studentID + " year pts changed oldV: "+ oldValue + ", newV: " + addNewValue + " - By teacher: " + UserInfo.getUser().getFullName());
			return true;
		} else {
			return false;
		}
	}
	
	@Deprecated
	public boolean addExamPointsSQL(String oldValue, String addNewValue, String subjectID, String studentID, String succ) throws SQLException {
		String spravil = "0";
		if (succ.length() > 0 && Integer.valueOf(succ) > 0) {
			spravil = "1";
		}
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			String query = "UPDATE zapisany_predmet SET body_skuska =" 
					+ addNewValue + ", spravil_predmet= " + spravil + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Student: " + studentID + " Exam pts set oldV: "+ oldValue + ", newV: " + addNewValue + ", succ: "+ spravil +" - By teacher: " + UserInfo.getUser().getFullName());
			return true;
		} else if (spravil.equalsIgnoreCase("1")){
			String query = "UPDATE zapisany_predmet SET spravil_predmet= " + spravil + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			Loggers.getLoggers().SUBJECT_LOGGER.log(Level.INFO, "Student: " + studentID + " Subject finalisation set: " + spravil + " - By teacher: " + UserInfo.getUser().getFullName());
			return true;
		}
		return false;
	}
	
	public boolean addExamPoints(String oldValue, String addNewValue, String subjectID, String studentID, String succ) {
		int spravil = 0;
		if (succ.length() > 0 && Integer.valueOf(succ) > 0) {
			spravil = 1;
		}
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			Session session = DatabaseConnection.getMe().getSessionFactory().getCurrentSession();
			final Transaction tx = session.beginTransaction();
			
			try {
				@SuppressWarnings("unchecked")
				List<Zapisany_predmet> predmety = session.createCriteria(Zapisany_predmet.class)
				.add(Restrictions.eq("predmet.predmetID", new Integer(subjectID)))
				.add(Restrictions.eq("student.id", new Integer(studentID)))
				.list();
				predmety.get(0).setBody_skuska(predmety.get(0).getBody_skuska() + Integer.valueOf(addNewValue));
				if (spravil==1) {
					predmety.get(0).setSpravil_predmet(true);
				}
				session.update(predmety.get(0));
				tx.commit();
			} catch (HibernateException e) {
				tx.rollback();
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Hibernate Problems in: " + this.getClass().getName(), e);
				return false;
			}
			Loggers.getLoggers().TEACHER_LOGGER.log(Level.INFO, "Student: " + studentID + " Exam pts set oldV: "+ oldValue + ", newV: " + addNewValue + ", succ: "+ spravil +" - By teacher: " + UserInfo.getUser().getFullName());
			return true;
		} else {
			return false;
		}
	}
	
}
