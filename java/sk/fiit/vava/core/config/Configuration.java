package sk.fiit.vava.core.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;
import java.util.logging.Level;


import javafx.fxml.FXMLLoader;

public class Configuration {
	/**
	 * UTF8Control class extending class Control used for ResourceBundle to control input stream from resources file<br>
	 * This class controls UTF8 charset files. Opens {@link InputStreamReader} with parameter "UTF-8".<br>
	 * {@link http://stackoverflow.com/questions/4659929/how-to-use-utf-8-in-resource-properties-with-resourcebundle}
	 * @author Lukas
	 *
	 */
	private class UTF8Control extends Control {
		public ResourceBundle newBundle(String baseName, Locale locale,
				String format, ClassLoader loader, boolean reload)
				throws IllegalAccessException, InstantiationException,
				IOException {
			// The below is a copy of the default implementation.
			String bundleName = toBundleName(baseName, locale);
			String resourceName = toResourceName(bundleName, "properties");
			ResourceBundle bundle = null;
			InputStream stream = null;
			if (reload) {
				URL url = loader.getResource(resourceName);
				if (url != null) {
					URLConnection connection = url.openConnection();
					if (connection != null) {
						connection.setUseCaches(false);
						stream = connection.getInputStream();
					}
				}
			} else {
				stream = loader.getResourceAsStream(resourceName);
			}
			if (stream != null) {
				try {
					// Only this line is changed to make it to read properties
					// files as UTF-8.
					bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
				} finally {
					stream.close();
				}
			}
			return bundle;
		}
	}
	
	
	private static final Configuration CFG = new Configuration();
		
	public static int student;
	public static int ucitel;
	public static int admin;
//	private static String url;
//	private static String dbName;
//	private static String driver;
//	private static String userName;
//	private static String password;
	
	private Configuration() {
		
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream("resources/database.properties");
			prop.load(input);
			student = Integer.valueOf(prop.getProperty("student"));
			ucitel = Integer.valueOf(prop.getProperty("ucitel"));
			admin = Integer.valueOf(prop.getProperty("admin"));
//			url = prop.getProperty("url");
//			dbName = prop.getProperty("dbName");
//			driver = prop.getProperty("driver");
//			userName = prop.getProperty("userName");
//			password = prop.getProperty("password");
			
		} catch (IOException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Resources file read problems in: " + this.getClass().getName(), e);
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Resources file read problems in: " + this.getClass().getName(), e);
			}
		}
		
	}
	
	public static Configuration getCfg() {
		return CFG;
	}
//	public String getUrl() {
//		return url;
//	}
//	public String getDbname() {
//		return dbName;
//	}
//	public String getDriver() {
//		return driver;
//	}
//	public String getUsername() {
//		return userName;
//	}
//	public String getPassword() {
//		return password;
//	}
	
	private Locale locale = Locale.getDefault();
	public void setLocale(String lang) {
		this.locale = new Locale(lang.toLowerCase(),lang.toUpperCase());
	}
	
	private ResourceBundle bundle;
	public ResourceBundle resourceBundle() {
		return bundle;
	}
	private FXMLLoader fxmlLoader;
	private UTF8Control utf8control = new UTF8Control();
	public FXMLLoader fxmlLoader() {
		fxmlLoader = new FXMLLoader();
		fxmlLoader.setResources(ResourceBundle.getBundle("sk.fiit.vava.resources.Resources", locale, utf8control));
		bundle = fxmlLoader.getResources();
		return fxmlLoader;
	}

	
	/**
	 * Method from stackoverflow WhiteFang34<br>
	 * {@link http://stackoverflow.com/questions/5683327/how-to-generate-a-random-string-of-20-characters}
	 * @return new Random password
	 */
	public static String getRandomPass() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMOPQRSTUVWXYZ".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
	
}
