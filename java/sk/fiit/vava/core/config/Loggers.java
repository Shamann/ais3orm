package sk.fiit.vava.core.config;

import java.io.IOException;
//import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Loggers {

	
	
	private static final Loggers loggers = new Loggers();
	private Handler guiHandler;
	private Handler lgHandler;
	private Handler thHandler;
	private Handler sbjHandler;
	private Handler stHandler;
	/**
	 * Logger handling GUI exceptions and errors to logs/errGuiLog.log
	 */
	public final Logger GUI_LOGGER = Logger.getLogger("Gui_Logger");
	/**
	 * Logger handling Logical and database errors to logs/errSystemLog.log
	 */
	public final Logger LG_LOGGER = Logger.getLogger("Logic_Logger");
	/**
	 * Logger handling Teacher changes to logs/teacherLog.log
	 */
	public final Logger TEACHER_LOGGER = Logger.getLogger("Teacher_Logger");
	/**
	 * Logger handling Subject changes to logs/subjectLog.log
	 */
	public final Logger SUBJECT_LOGGER = Logger.getLogger("Subject_Logger");
	/**
	 * Logger handling Student changes to logs/studentLog.log
	 */
	public final Logger STUDENT_LOGGER = Logger.getLogger("Student_Logger");
	
	private Loggers() {
		
		try {
			guiHandler = new FileHandler("logs/errGuiLog.log",true);
			lgHandler = new FileHandler("logs/errSystemLog.log",true);
			thHandler = new FileHandler("logs/teacherLog.log",true);
			sbjHandler = new FileHandler("logs/subjectLog.log",true);
			stHandler = new FileHandler("logs/studentLog.log",true);
			
			guiHandler.setFormatter(new SimpleFormatter());
			lgHandler.setFormatter(new SimpleFormatter());
			thHandler.setFormatter(new SimpleFormatter());
			sbjHandler.setFormatter(new SimpleFormatter());
			stHandler.setFormatter(new SimpleFormatter());
			
			GUI_LOGGER.addHandler(guiHandler);
			LG_LOGGER.addHandler(lgHandler);
			TEACHER_LOGGER.addHandler(thHandler);
			SUBJECT_LOGGER.addHandler(sbjHandler);
			STUDENT_LOGGER.addHandler(stHandler);
			
			GUI_LOGGER.setLevel(Level.WARNING);
			LG_LOGGER.setLevel(Level.WARNING);
			SUBJECT_LOGGER.setLevel(Level.FINE);
			TEACHER_LOGGER.setLevel(Level.FINE);
			STUDENT_LOGGER.setLevel(Level.FINE);
			
//			GUI_LOGGER.setResourceBundle(ResourceBundle.getBundle("resources.GuiConfig"));
//			LG_LOGGER.setResourceBundle(ResourceBundle.getBundle("resources.LgConfig"));
//			TEACHER_LOGGER.setResourceBundle(ResourceBundle.getBundle("resources.TeacherConfig"));
//			SUBJECT_LOGGER.setResourceBundle(ResourceBundle.getBundle("resources.SubjectConfig"));
//			STUDENT_LOGGER.setResourceBundle(ResourceBundle.getBundle("resources.StudentConfig"));
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static Loggers getLoggers() {
		return loggers;
	}
	
	public void closeLoggers() {
		guiHandler.close();
		lgHandler.close();
		thHandler.close();
		sbjHandler.close();
		stHandler.close();
	}
}
