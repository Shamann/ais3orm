package sk.fiit.vava.core.database;

import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.logging.Level;

import org.hibernate.SessionFactory;

//import sk.fiit.vava.core.config.Configuration;
//import sk.fiit.vava.core.config.Loggers;


public class DatabaseConnection {

private SessionFactoryUtil session = new SessionFactoryUtil();
	
	private static final DatabaseConnection dbconn = new DatabaseConnection();

	private Connection conn;
	
	private DatabaseConnection() {
//		String driver = Configuration.getCfg().getDriver();
//		String database = Configuration.getCfg().getUrl() + Configuration.getCfg().getDbname();
//		
//		try {
//			Class.forName(driver).newInstance();
//			
//			conn = DriverManager.getConnection(database, Configuration.getCfg().getUsername(), Configuration.getCfg().getPassword());
//			
//		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
//			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Connection Problems in: " + this.getClass().getName(), e);
//			e.printStackTrace();
//		}
	}
	public static DatabaseConnection getMe() {
		return dbconn;
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public SessionFactory getSessionFactory() {
		return session.getSessionFactory();
	}
	public SessionFactoryUtil getSessionFactoryUtil() {
		return session;
	}
	
	public void close() {
		DatabaseConnection.getMe().getSessionFactoryUtil().shutDown();
//		try {
//			conn.close();
//		} catch (SQLException e) {
//			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Connection Problems in: " + this.getClass().getName(), e);
//			e.printStackTrace();
//		}
	}
	
	/**
	 * makes database call and returns results
	 * @param query String
	 * @return ResultSet - result of sent query
	 * @throws SQLException
	 */
	public ResultSet makeDatabaseCall(String query) throws SQLException {
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		
		return rs;
	}
	
	public ResultSet makeDatabaseUpdate(String query) throws SQLException {
		
		Statement stmt = conn.createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);
		stmt.executeUpdate(query);
		ResultSet res = stmt.getResultSet();
		DatabaseConnection.getMe().getConnection().commit();
		return res;
	}
	

}
