package sk.fiit.vava.core.database;

import java.util.logging.Level;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import sk.fiit.vava.core.config.Loggers;

public class SessionFactoryUtil {

	private SessionFactory sessionFactory;
	
	public SessionFactoryUtil() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure("/sk/fiit/vava/core/config/hibernate.cfg.xml");
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.SEVERE, "Error by SessionFactory creation", e);
		}
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public void shutDown() {
		sessionFactory.close();
	}
}
