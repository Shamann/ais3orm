package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.models.Login;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoginController implements Initializable {

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	
	@FXML
	AnchorPane loginAnchor;
	
	@FXML
	private Label wrongLogin;
	@FXML
	private TextField nameFld;
	@FXML
	private TextField passFld;
	@FXML
	private Button langBtn;
	@FXML
	private Button loginBtn;
	
	public void logIn() {

		Login loginVerifier = new Login();	
		final String name = nameFld.getText();
		final String passwd = passFld.getText();

		passFld.setDisable(true);
		nameFld.setDisable(true);
		loginBtn.setDisable(true);
		langBtn.setDisable(true);
		
		Task<Boolean> login = new Task<Boolean>(){

			@Override
			protected Boolean call() throws Exception {
				return loginVerifier.verifyLogin(name, passwd);
			}
			
		};

		login.setOnFailed(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				login.getException().printStackTrace();
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
				nameFld.setDisable(false);
				passFld.setDisable(false);
				loginBtn.setDisable(false);
				langBtn.setDisable(false);
			}
		});
		
		login.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				
				if (login.getValue()) {
					try {
							if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
								loginAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/TeacherMainView.fxml").openStream())));
								wrongLogin.setVisible(false);
							} else 
							if (UserInfo.getUser().getUserID() >= Configuration.admin) {
								loginAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/AdminMainView.fxml").openStream())));
								wrongLogin.setVisible(false);
							} else 
							if (UserInfo.getUser().getUserID() >= Configuration.student) {
								loginAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MainView.fxml").openStream())));
								wrongLogin.setVisible(false);
							} 
							
						} catch (IOException e) {
							Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
						}
					} else {
						wrongLogin.setVisible(true);
						nameFld.setDisable(false);
						passFld.setDisable(false);
						loginBtn.setDisable(false);
						langBtn.setDisable(false);
						passFld.clear();
					}
				
			}
		});
		new Thread(login).start();
		
	}
	
	@FXML
	public void changeLocal() {
		Configuration.getCfg().setLocale(langBtn.getText());
		try {
			loginAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/LoginView.fxml").openStream())));
			((Stage) loginAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem reloading Login Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}

}
