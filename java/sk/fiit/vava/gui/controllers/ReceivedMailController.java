package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.MailTable;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.models.MailManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

public class ReceivedMailController implements Initializable{

	@FXML
	private AnchorPane mainPane;
	
	@FXML
    private TableColumn<MailTable, Integer> sizeC;

    @FXML
    private TableColumn<MailTable, String> subjectC;

    @FXML
    private TableColumn<MailTable, String> dateC;

    @FXML
    private TableView<MailTable> mailTable;

    @FXML
    private TableColumn<MailTable, String> fromC;

    @FXML
    private TableColumn<MailTable, String> seenC;

    @FXML
    private TextArea textArea;

    private MailManager mm = new MailManager();
    
    @FXML
    void deleteMail(ActionEvent event) {
    	if (mailTable.getSelectionModel().getSelectedItem()!=null) {
	    	mm.deleteMail(mailTable.getSelectionModel().getSelectedItem().getId(),UserInfo.getUser().getUserID());
	    	fillMails();
    	}
    }
	
    @FXML
    void answerMail(ActionEvent event) {
    	MailTable mt = mailTable.getSelectionModel().getSelectedItem();
    	if (mt!=null) {
    		EmailUtil.getEmailUtil().setMailTo(mt.getFrom());
    		EmailUtil.getEmailUtil().setText(mt.getText(),mt.getSent());
    		EmailUtil.getEmailUtil().setSubject(mt.getSubject());
    		try{
    			mainPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/SendNewMailView.fxml").openStream())));
			} catch (IOException e) {
				Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
			}
    	}
    }

    @FXML
    void forwardMail(ActionEvent event) {
    	MailTable mt = mailTable.getSelectionModel().getSelectedItem();
    	if (mt!=null) {
    		EmailUtil.getEmailUtil().setText(mt.getText(),mt.getSent());
    		
    		try{
    			mainPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/SendNewMailView.fxml").openStream())));
			} catch (IOException e) {
				Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
			}
    	}
    }
    
    private void fillMails() {
    	Task<ObservableList<MailTable>> task = new Task<ObservableList<MailTable>>() {

			@Override
			protected ObservableList<MailTable> call() throws Exception {
				return mm.getReceivedMails(UserInfo.getUser().getUserID(), UserInfo.getUser().getMail());
			}
		};
		
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				mailTable.setItems(task.getValue());
		    	dateC.setCellValueFactory(MailTable.getSentProperty());
		    	fromC.setCellValueFactory(MailTable.getFromProperty());
		    	subjectC.setCellValueFactory(MailTable.getSubjectProperty());
				sizeC.setCellValueFactory(MailTable.getSizeProperty());
				seenC.setCellValueFactory(MailTable.getSeenProperty());
				
			}
		});    	
		new Thread(task).start();
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillMails();
		
		mailTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MailTable>() {

			@Override
			public void changed(ObservableValue<? extends MailTable> observable, MailTable oldValue, MailTable newValue) {
				MailTable mt = mailTable.getSelectionModel().getSelectedItem(); 
				if (mt!=null) {
					textArea.setText(mt.getText());
					mt.setSeen(true);
					mm.setSeen(mt.getId());
				}
			}
			
		});
	}

}
