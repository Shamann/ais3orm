package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TeacherMainController implements Initializable {

	@FXML
	public AnchorPane teacherMainPane; 
	@FXML
	public GridPane teacherPane;
	@FXML
	public Label nameLabel;
	@FXML
	public Label statusLabel;
	@FXML
	public Button langBtn;
	@FXML
	public Button mailBtn;
	
	private Task<Long> task = new Task<Long>() {

		@Override
		protected Long call() throws Exception {
			while (EmailUtil.getEmailUtil().getNewMails() == 0) {
				Thread.sleep(1000);
			}
			return EmailUtil.getEmailUtil().getNewMails();
		}
	};
	private Thread th = new Thread(task);
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			teacherPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudiesManagementView.fxml").openStream())));
			nameLabel.setText(Configuration.getCfg().resourceBundle().getString("lgInAs")
					+ UserInfo.getUser().getName() + " "
					+ UserInfo.getUser().getSurname() + ", ID: "
					+ UserInfo.getUser().getUserID());
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				mailBtn.setText(mailBtn.getText()+" :"+task.getValue());
				th.run();
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), task.getException());
				th.run();
			}
		});
		
		th.setDaemon(true);
		th.start();
	}

	@FXML
	private void studiesManagement() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("teacherMC1"));
			teacherPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudiesManagementView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}

	@FXML
	private void manageSubjects() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("teacherMC2"));
			teacherPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/LectorManagementView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}
	
	@FXML
	private void subjectStats() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("teacherMC3"));
			teacherPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/SubjectStatisticsView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}

	@FXML
	private void changeLocal() {
		Configuration.getCfg().setLocale(langBtn.getText());
		try {
			teacherMainPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/TeacherMainView.fxml").openStream())));
			((Stage) teacherMainPane.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem reloading Teacher Main Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	private void openMails(ActionEvent event) {
		try {
			teacherMainPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MailboxMainView.fxml").openStream())));
			((Stage) teacherMainPane.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem Mail Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	private void logOut() {
		DatabaseConnection.getMe().close();
		Loggers.getLoggers().closeLoggers();
		System.exit(0);
	}

}
