package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import sk.fiit.vava.core.dataObjects.SubjectList;
import sk.fiit.vava.core.models.SubjectManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class StudentEIndexController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		subjectTable.setItems(sm.getStudentSubjectStats("" + UserInfo.getUser().getUserID()));
		subjectID.setCellValueFactory(SubjectList.getIDproperty());
		subjectName.setCellValueFactory(SubjectList.getNameProperty());
		subjectCredits.setCellValueFactory(SubjectList.getCreditsProperty());
		subjectYear.setCellValueFactory(SubjectList.getTestProperty());
		subjectExam.setCellValueFactory(SubjectList.getExamProperty());
		
	}

    @FXML
    private TableColumn<SubjectList, Integer> subjectYear;

    @FXML
    private TableColumn<SubjectList, Integer> subjectCredits;

    @FXML
    private TableView<SubjectList> subjectTable;

    @FXML
    private TableColumn<SubjectList, Integer> subjectID;

    @FXML
    private TableColumn<SubjectList, Integer> subjectExam;

    @FXML
    private TableColumn<SubjectList, String> subjectName;

	
	
	private SubjectManager sm = new SubjectManager();
}
