package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;

import sk.fiit.vava.core.databaseTableObjects.Predmet;
import sk.fiit.vava.core.models.SubjectManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

public class StudentSubjectManagementController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillSubjectLV();
		fillStudentSubjects();
		createHashMapOfSubjects(subjectManager.getSubjects());
		
		allSubjectsLv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (allSubjectsLv.getSelectionModel().getSelectedItem() != null) {
					String subjectID = allSubjectsLv.getSelectionModel().getSelectedItem().split(" ")[0];
					infoArea.setText(getSubjectInfo(Integer.valueOf(subjectID)));
					successInsLbl.setVisible(false);
					if(subjectManager.hasStudentSubject(UserInfo.getUser().getUserID() +"" , subjectID)) {
						applyAddBtn.setDisable(true);
					} else {
						applyAddBtn.setDisable(false);
					}
				}
			}
		});
	}

	
	private void createHashMapOfSubjects(final List<Predmet> subjects) {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {

				for (Predmet p : subjects) {
					StudentSubjectManagementController.this.subjects.put(p.getPredmetID(), p);
				}
				return null;
			}
		};
		new Thread(task).start();
	}


	private Hashtable<Integer, Predmet> subjects = new Hashtable<Integer, Predmet>();
	private ObservableList<String> subjectList = FXCollections.observableArrayList();
	
	private String getSubjectInfo(int subjectID) {
		String output = "";
		output = subjects.get(subjectID).getPopis();
		return output;
	}
	
	@FXML
	private ListView<String> studentSubjectLv;
	@FXML
	private ListView<String> allSubjectsLv;
	@FXML
	private Button applyAddBtn;
	@FXML
	private Label successInsLbl;
	@FXML
	private TextArea infoArea;
	
	@FXML
	private void applyInsert() {
		subjectManager.subscribeSubject(UserInfo.getUser().getUserID()+"", allSubjectsLv.getSelectionModel().getSelectedItem().split(" ")[0], "1", "1");
		fillStudentSubjects();
		successInsLbl.setVisible(true);
		
	}
	
	SubjectManager subjectManager = new SubjectManager();
	
	private void fillSubjectLV() {
		final Task<Void> showSubjcts = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<Predmet> subjects = subjectManager.getSubjects();
				createHashMapOfSubjects(subjects);
				for (Predmet predmet : subjects) {
					subjectList.add(predmet.getPredmetID() + " - " + predmet.getMeno() + " k: " + predmet.getKredity());
				}
				allSubjectsLv.setItems(subjectList);
				return null;
			};
		};
		new Thread(showSubjcts).start();
	}
	
	private void fillStudentSubjects() {
			
			final Task<Void> showStudentSubjects = new Task<Void>() {
	
				@Override
				protected Void call() throws Exception {
					studentSubjectLv.setItems(subjectManager.getStudentSubjects(UserInfo.getUser().getUserID()+""));
					return null;
				};
				
			};
			new Thread(showStudentSubjects).start();
			
		}
}
