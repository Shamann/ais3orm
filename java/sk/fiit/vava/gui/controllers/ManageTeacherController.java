package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.TeacherTable;
import sk.fiit.vava.core.models.ManageTeachers;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

public class ManageTeacherController implements Initializable {
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		teacherTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TeacherTable>() {
			@Override
			public void changed(ObservableValue<? extends TeacherTable> observable, TeacherTable oldValue, TeacherTable newValue) {
				if (teacherTable.getSelectionModel().getSelectedItem()!=null) {
					setFields();
				}
			}       
		});
		fillTeacherTable();
	}
	
	private void setFields() {
		if (teacherTable.getSelectionModel().getSelectedItem() != null) {
			nameUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getName());
			nameDel.setText(teacherTable.getSelectionModel().getSelectedItem().getName());
			surnameUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getSurname());
			surnameDel.setText(teacherTable.getSelectionModel().getSelectedItem().getSurname());
			teacherIdDel.setText(teacherTable.getSelectionModel().getSelectedItem().getIdentificator() + "");
			teacherSinceUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getTeachSince());
			mailUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getEmail());
		}
	}

	@FXML
	private TextField nameAdd;
	@FXML
	private TextField surnameAdd;
	@FXML
	private TextField teacherSinceAdd;
	@FXML
	private TextField mailAdd;
	@FXML
	private TextField nameUpd;
	@FXML
	private TextField surnameUpd;
	@FXML
	private TextField teacherSinceUpd;
	@FXML
	private TextField mailUpd;
	@FXML
	private TextField newPasswd;
	@FXML
	private TextField nameDel;
	@FXML
	private TextField surnameDel;
	@FXML
	private TextField teacherIdDel;
	@FXML
	private Label successLbl;
	@FXML
	private Label passLbl;
	@FXML
	private TitledPane x3;
	
	@FXML
	private void invisibleSucces(){
		successLbl.setVisible(false);
		passLbl.setVisible(false);
	}
	
	@FXML
	private void addTeacher(ActionEvent event) {
				
		Task<String> task = new Task<String>() {

			@Override
			protected String call() throws Exception {
				return teacherManager.insertTeacher(nameAdd.getText(), surnameAdd.getText(), teacherSinceAdd.getText() , mailAdd.getText());
			}
		};
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				String passwd = task.getValue();
				clearAdd();
				fillTeacherTable();
				passLbl.setText(passwd);
				passLbl.setVisible(true);
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
				Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		new Thread(task).start();
	}
	
	@FXML
	private void updateTeacher(ActionEvent event) {
		if (teacherTable.getSelectionModel().getSelectedItem() != null) {
			Task<Boolean> task = new Task<Boolean>() {

				@Override
				protected Boolean call() throws Exception {	
					return teacherManager.updateTeacher(teacherTable.getSelectionModel().getSelectedItem().getIdentificator(), 
							nameUpd.getText(), 
							surnameUpd.getText(), 
							teacherSinceUpd.getText(), 
							mailUpd.getText(), 
							newPasswd.getText());
				}
				
			};
			
			task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

				@Override
				public void handle(WorkerStateEvent event) {
					if(task.getValue()) {
						fillTeacherTable();
						successLbl.setText("Update Successful!");
						successLbl.setVisible(true);
					}
				}
			});
			task.setOnFailed(new EventHandler<WorkerStateEvent>() {

				@Override
				public void handle(WorkerStateEvent event) {
					task.getException().printStackTrace();
					Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
				}
			});
			new Thread(task).start();
		}
		
		
	}
	
	@FXML
	private void deleteTeacher(ActionEvent event) {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				teacherManager.deleteTeacher(teacherTable.getSelectionModel().getSelectedItem().getName(), 
						teacherTable.getSelectionModel().getSelectedItem().getSurname(), 
						teacherTable.getSelectionModel().getSelectedItem().getIdentificator());
				return null;
			}
			
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
				Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				fillTeacherTable();
				nameDel.clear();
				surnameDel.clear();
				teacherIdDel.clear();
			}
		});
		new Thread(task).start();
	}

	@FXML
	private void clearAdd(){
		nameAdd.clear();
		surnameAdd.clear();
		teacherSinceAdd.clear();
		mailAdd.clear();
	}
	@FXML
	private void cancelAdd(ActionEvent event) {
		clearAdd();
	}
	
	@FXML
	private void cancelUpdate(ActionEvent event) {
		setFields();
	}
	
	@FXML
	private void cancelDelete(ActionEvent event) {
		nameDel.clear();
		surnameDel.clear();
		teacherIdDel.clear();
	}

	@FXML
	private void confirmCancel() {
		// TODO confirm cancel teacher
	}
	
	@FXML
	private void denyCancel() {
		// TODO deny cancel teacher
	}
	
	@FXML
	public TableView<TeacherTable> teacherTable;
	@FXML
	public TableColumn<TeacherTable, Integer> teacherID;
	@FXML
	public TableColumn<TeacherTable, String> nameCol;
	@FXML
	public TableColumn<TeacherTable, String> surnameCol;
	@FXML
	public TableColumn<TeacherTable, String> teacherSinceCol;
	@FXML
	public TableColumn<TeacherTable, String> emailCol;
	
	
	private ObservableList<TeacherTable> teacherList = FXCollections.observableArrayList();
	private ManageTeachers teacherManager = new ManageTeachers();
	
	private void fillTeacherTable() {
		teacherList.clear();
		teacherList = teacherManager.loadTeachersToTable();
		teacherTable.setItems(teacherList);
		teacherID.setCellValueFactory(TeacherTable.getIdentificatorProperty());
		nameCol.setCellValueFactory(TeacherTable.getNameProperty());
		surnameCol.setCellValueFactory(TeacherTable.getSurnameNameProperty());
		teacherSinceCol.setCellValueFactory(TeacherTable.getTeachSinceProperty());
		emailCol.setCellValueFactory(TeacherTable.getMailProperty());
		
	}
}
