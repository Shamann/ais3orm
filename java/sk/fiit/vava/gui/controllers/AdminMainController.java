package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.database.DatabaseConnection;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class AdminMainController implements Initializable{

	@FXML
	public GridPane adminPane;
	@FXML
	public AnchorPane adminAnchor;
	@FXML
	public Label nameLabel;
	@FXML
	public Label statusLabel;
	@FXML
	private Button langBtn;
	@FXML
	private Button mailBtn;

	private Task<Long> task = new Task<Long>() {

		@Override
		protected Long call() throws Exception {
			while (EmailUtil.getEmailUtil().getNewMails() == 0) {
				Thread.sleep(1000);
			}
			return EmailUtil.getEmailUtil().getNewMails();
		}
	};
	private Thread th = new Thread(task);
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			adminPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/ManageStudentView.fxml").openStream())));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
			e.printStackTrace();
		}
		nameLabel.setText(Configuration.getCfg().resourceBundle().getString("lgInAs")
				+ UserInfo.getUser().getName() + " "
				+ UserInfo.getUser().getSurname() + ", ID: "
				+ UserInfo.getUser().getUserID());
		
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				mailBtn.setText(mailBtn.getText()+" :"+task.getValue());
				th.run();
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), task.getException());
				th.run();
			}
		});
		
		th.setDaemon(true);
		th.start();
	}

	@FXML
	private void manageStudents() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("adminMC2"));
			adminPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/ManageStudentView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void manageSubjects() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("adminMC3"));
			adminPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/ManageSubjectView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void manageTeachers() {
		try {
			statusLabel.setText(Configuration.getCfg().resourceBundle().getString("adminMC4"));
			adminPane.getChildren().setAll((GridPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/ManageTeacherView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void changeLocal(ActionEvent event) {
		Configuration.getCfg().setLocale(langBtn.getText());
		try {
			adminAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/AdminMainView.fxml").openStream())));
			((Stage) adminAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem reloading Admin Main Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}

	@FXML
	private void openMails(ActionEvent event) {
		try {
			adminAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MailboxMainView.fxml").openStream())));
			if (th.isAlive()) {
				th.interrupt();
			}
			((Stage) adminAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem Mail Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	private void logOut() {
		DatabaseConnection.getMe().close();
		Loggers.getLoggers().closeLoggers();
		System.exit(0);
	}
	
}
