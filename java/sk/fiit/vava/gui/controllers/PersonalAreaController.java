package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PersonalAreaController implements Initializable {

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			innerModel.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudentStudyDetailsView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e);
			e.printStackTrace();
		}
		
		nameLbl.setText(Configuration.getCfg().resourceBundle().getString("lgInAs")
				+ UserInfo.getUser().getName() + " "
				+ UserInfo.getUser().getSurname() + ", ID: "
				+ UserInfo.getUser().getUserID());
		
		
		
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				mailBtn.setText(mailBtn.getText()+" :"+task.getValue());
				th.run();
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), task.getException());
				th.run();
			}
		});
		
		th.setDaemon(true);
		th.setName("MailThread");
		th.start();
	}
	
	private Task<Long> task = new Task<Long>() {

		@Override
		protected Long call() throws Exception {
			while (EmailUtil.getEmailUtil().getNewMails() == 0) {
				Thread.sleep(1000);
			}
			return EmailUtil.getEmailUtil().getNewMails();
		}
	};
	private Thread th = new Thread(task);
	@FXML
	private AnchorPane innerModel;
	@FXML
	private AnchorPane personalViewAnchor;
	@FXML
	private Label nameLbl;
	@FXML 
	private Button langBtn;
	@FXML
	private Button mailBtn;
	
	@FXML
	public void openMails() {
		try {
			personalViewAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MailboxMainView.fxml").openStream())));
			((Stage) personalViewAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem Mail Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showEIndex() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudentEIndexView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showSchoolmates() {

	}
	
	@FXML
	public void showStudyDetails() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudentStudyDetailsView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showStudyPlan() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/StudentSubjectManagementView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showLectureSheet() {

	}
	
	@FXML
	public void changeLocal() {
		Configuration.getCfg().setLocale(langBtn.getText());
		try {
			personalViewAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/PersonalAreaView.fxml").openStream())));
			if (th.isAlive()) {
				th.interrupt();
			}
			((Stage) personalViewAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem reloading Personal Area Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	public void toMainView() {
		try {
			personalViewAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MainView.fxml").openStream())));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "GUI Problems in: " + this.getClass().getName(), e.getCause());
			e.printStackTrace();
		}
	}

}
