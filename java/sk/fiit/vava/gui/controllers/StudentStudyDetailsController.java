package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import sk.fiit.vava.core.models.StudentDetailsManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class StudentStudyDetailsController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		showStatistics();
		showSubjects();
	}
	
	@FXML
	private ListView<String> subjectList;
	@FXML
	private Label done;
	@FXML
	private Label idLbl;
	@FXML
	private Label nameLbl;
	@FXML
	private Label yearLbl;
	@FXML
	private Label periodLbl;
	@FXML
	private Label facultyLbl;
	@FXML
	private Label allLbl;
	@FXML
	private Label finishedLbl;
	@FXML
	private Label creditsLbl;

	private StudentDetailsManager sdm = new StudentDetailsManager();
	
	public void showStudentSubjects() {
		
	}
	
	private void showStatistics() {
		String[] labels = sdm.getStatistics(UserInfo.getUser().getUserID());
		idLbl.setText(labels[0]);
		nameLbl.setText(labels[1]);
		yearLbl.setText(labels[2]);
		periodLbl.setText(labels[3]);
		facultyLbl.setText(labels[4]);
		allLbl.setText(labels[5]);
		finishedLbl.setText(labels[6]);
		creditsLbl.setText(labels[7]);
	}
	
	private void showSubjects() {
		subjectList.setItems(sdm.getStudentSubjects(UserInfo.getUser().getUserID() + ""));
	}
}
