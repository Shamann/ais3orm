package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import sk.fiit.vava.core.dataObjects.SubjectList;
import sk.fiit.vava.core.models.SubjectManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class SubjectStatisticsController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		subjectTv.setItems(sm.getSubjectStatistics());
		subjectID.setCellValueFactory(SubjectList.getIDproperty());
		subjectName.setCellValueFactory(SubjectList.getNameProperty());
		subscribers.setCellValueFactory(SubjectList.getSubscribersProperty());		
	}

	private SubjectManager sm = new SubjectManager();
    @FXML
    private TableColumn<SubjectList, Integer> subscribers;

    @FXML
    private TableView<SubjectList> subjectTv;

    @FXML
    private TableColumn<SubjectList, Integer> subjectID;

    @FXML
    private TableColumn<SubjectList, String> subjectName;
}
