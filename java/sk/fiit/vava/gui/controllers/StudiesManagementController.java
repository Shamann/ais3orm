package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.StudentTable;
import sk.fiit.vava.core.models.ManageStudents;
import sk.fiit.vava.core.models.SubjectManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class StudiesManagementController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		try {
			subjects = sm.getLectorSubjects(UserInfo.getUser().getUserID());
		} catch (SQLException e) {
			Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "SQL Problems in: " + this.getClass().getName(), e);
			e.printStackTrace();
		}
		
		subjectCB.setItems(subjects);
		subjectCB.setVisibleRowCount(5);
		subjectCB.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				fillStudentTable(subjectCB.getSelectionModel().getSelectedItem().split(" ")[0]);
				
			}
		});
		
		studentProgress.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StudentTable>() {

			@Override
			public void changed(ObservableValue<? extends StudentTable> arg0, StudentTable arg1, StudentTable arg2) {
				if (studentProgress.getSelectionModel().getSelectedItem() != null) {
					setStudentFields(studentProgress.getSelectionModel().getSelectedItem());
				}
			}
		});
	}

	private void fillStudentTable(String subjectID) {
		studentProgress.getSelectionModel().clearSelection();
		Task<ObservableList<StudentTable>> task = new Task<ObservableList<StudentTable>>() {

			@Override
			protected ObservableList<StudentTable> call() throws Exception {
				return ms.getStudentsFromSubject(subjectID);
			}
			
		};
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				studentsFromSubject = task.getValue();
				studentProgress.setItems(studentsFromSubject);
				
				idCol.setCellValueFactory(StudentTable.getIdentificatorProperty());
				nameCol.setCellValueFactory(StudentTable.getWholeNameProperty());
				ptsCol.setCellValueFactory(StudentTable.getPointsProperty());
				exmCol.setCellValueFactory(StudentTable.getExamProperty());
				yearCol.setCellValueFactory(StudentTable.getYearProperty());
				perCol.setCellValueFactory(StudentTable.getPeriodProperty());
				succCol.setCellValueFactory(StudentTable.getSuccProperty());
				// TODO add sum of points
				
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
				Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		new Thread(task).start();
	}

	@FXML
	private Label succYear;
	@FXML
	private Label succExam;
	@FXML
	private ComboBox<String> subjectCB;
	@FXML
	private TableView<StudentTable> studentProgress;
	@FXML
	private TableColumn<StudentTable, Integer> idCol;
	@FXML
	private TableColumn<StudentTable, String> nameCol;
	@FXML
	private TableColumn<StudentTable, Integer> ptsCol;
	@FXML
	private TableColumn<StudentTable, Integer> exmCol; 
	@FXML
	private TableColumn<StudentTable, Integer> yearCol;
	@FXML
	private TableColumn<StudentTable, Integer> perCol;
	@FXML
	private TableColumn<StudentTable, Integer> succCol;

	private ObservableList<String> subjects;
	private ObservableList<StudentTable> studentsFromSubject;
	private SubjectManager sm = new SubjectManager();
	private ManageStudents ms = new ManageStudents();
	
	@FXML
	public void stashYear() {
		succYear.setVisible(false);
	}
	@FXML
	public void stashExam() {
		succExam.setVisible(false);
	}
	
	@FXML
	private TextField idE;
	@FXML
	private TextField nameE;
	@FXML
	private TextField ptsYearE;
	@FXML
	private TextField ptsExamE;
	@FXML
	private TextField idY;
	@FXML
	private TextField nameY;
	@FXML
	private TextField tillY;
	@FXML
	private TextField newY;
	@FXML
	private TextField succ;
	
	private void setStudentFields(StudentTable student) {
		idE.setText(student.getIdentificator()+"");
		idY.setText(student.getIdentificator()+"");
		nameE.setText(student.getWholeName());
		nameY.setText(student.getWholeName());
		ptsYearE.setText(student.getPoints()+"");
		tillY.setText(student.getPoints()+"");		
	}
	
	@FXML
	public void applyYearPoints() {
		int oldV = Integer.valueOf(tillY.getText());
		int newV = Integer.valueOf(newY.getText());
		Task<Boolean> task = new Task<Boolean>() {

			@Override
			protected Boolean call() throws Exception {
				return ms.addYearPoints(tillY.getText(), newY.getText(), subjectCB.getSelectionModel().getSelectedItem().split(" ")[0], idY.getText());
			}
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				if (task.getValue()) {
					fillStudentTable(subjectCB.getSelectionModel().getSelectedItem().split(" ")[0]);
					succYear.setVisible(true);
					ptsYearE.setText((oldV+newV) + "");
					tillY.setText((oldV+newV) + "");
					newY.clear();
				}
			}
		});
		new Thread(task).start();
	}
	
	@FXML
	public void stashYearPoints() {
		newY.clear();
		succ.clear();
	}
	
	@FXML
	public void applyExamPoints () {
		Task<Boolean> task = new Task<Boolean>() {

			@Override
			protected Boolean call() throws Exception {
				return ms.addExamPoints(ptsYearE.getText(), ptsExamE.getText(), subjectCB.getSelectionModel().getSelectedItem().split(" ")[0], idE.getText(), succ.getText());
			}
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().GUI_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				if (task.getValue()) {
					fillStudentTable(subjectCB.getSelectionModel().getSelectedItem().split(" ")[0]);
					succExam.setVisible(true);
					newY.clear();	
				}
			}
		});
		new Thread(task).start();
	}
	
	@FXML
	public void stashExamPoints() {
		ptsExamE.clear();
		succ.clear();
	}
}
