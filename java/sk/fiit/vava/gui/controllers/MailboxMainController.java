package sk.fiit.vava.gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Configuration;
import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MailboxMainController implements Initializable {

	@FXML
	private AnchorPane mailAnchor;
	@FXML
	private AnchorPane mailPane;
	@FXML
	private Label nameLabel;
	@FXML
	private Label statsLabel;
	@FXML
	private Button langBtn;
	@FXML
	private Button back;
	@FXML
	private Button mailBtn;
	
	private Task<Long> task = new Task<Long>() {

		@Override
		protected Long call() throws Exception {
			while (EmailUtil.getEmailUtil().getNewMails() == 0) {
				Thread.sleep(1000);
			}
			return EmailUtil.getEmailUtil().getNewMails();
		}
	};
	private Thread th = new Thread(task);
	
	@FXML
	private void openReceived(ActionEvent event) {
		try {
			statsLabel.setText(Configuration.getCfg().resourceBundle().getString("received_mail"));
			mailPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/ReceivedMailView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void openSent(ActionEvent event) {
		try {
			EmailUtil.getEmailUtil().setMailTo("");
    		EmailUtil.getEmailUtil().setText("","");
    		EmailUtil.getEmailUtil().setSubject("");
			statsLabel.setText(Configuration.getCfg().resourceBundle().getString("sent_mail"));
			mailPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/SentMailView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void sendNew(ActionEvent event) {
		try {
			EmailUtil.getEmailUtil().setMailTo("");
    		EmailUtil.getEmailUtil().setText("","");
    		EmailUtil.getEmailUtil().setSubject("");
			statsLabel.setText(Configuration.getCfg().resourceBundle().getString("send_new_mail"));
			mailPane.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/SendNewMailView.fxml").openStream())));
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@FXML
	private void changeLocal(ActionEvent event) {
		Configuration.getCfg().setLocale(langBtn.getText());
		try {
			EmailUtil.getEmailUtil().setMailTo("");
    		EmailUtil.getEmailUtil().setText("","");
    		EmailUtil.getEmailUtil().setSubject("");
			mailAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MailboxMainView.fxml").openStream())));
			((Stage) mailAnchor.getScene().getWindow()).setTitle(Configuration.getCfg().resourceBundle().getString("asiMain1"));
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e) {
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problem reloading Mail main Page " + this.getClass().getName());
			e.printStackTrace();
		}
	}
	
	@FXML
	private void back(ActionEvent event) {
		try {
			EmailUtil.getEmailUtil().setMailTo("");
    		EmailUtil.getEmailUtil().setText("","");
    		EmailUtil.getEmailUtil().setSubject("");
			if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
				mailAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/TeacherMainView.fxml").openStream())));
			} else if (UserInfo.getUser().getUserID() >= Configuration.admin) {
				mailAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/AdminMainView.fxml").openStream())));
			} else if (UserInfo.getUser().getUserID() >= Configuration.student) {
				mailAnchor.getChildren().setAll((AnchorPane) (Configuration.getCfg().fxmlLoader().load(getClass().getResource("/sk/fiit/vava/gui/views/MainView.fxml").openStream())));
			}
			if (th.isAlive()) {
				th.interrupt();
			}
		} catch (IOException e){
			Loggers.getLoggers().GUI_LOGGER.log(Level.SEVERE, "Problems in: " + this.getClass().getName(), e);
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nameLabel.setText(Configuration.getCfg().resourceBundle().getString("lgInAs")
				+ UserInfo.getUser().getName() + " "
				+ UserInfo.getUser().getSurname() + ", ID: "
				+ UserInfo.getUser().getUserID());
		
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				mailBtn.setText(mailBtn.getText()+" :"+task.getValue());
				th.run();
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), task.getException());
				th.run();
			}
		});
		
		th.setDaemon(true);
		th.start();
		
		openReceived(new ActionEvent());
		
	}

}
