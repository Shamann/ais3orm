package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.mail.EmailUtil;
import sk.fiit.vava.core.models.MailManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class SendNewMailController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fromLabel.setText(UserInfo.getUser().getMail());
		forField.setText(EmailUtil.getEmailUtil().getMailTo());
		subjectField.setText(EmailUtil.getEmailUtil().getSubject());
		textArea.setText(EmailUtil.getEmailUtil().getText());
	}
	
	@FXML
	private Button sendBtn;
	
	@FXML
    private Label fromLabel;

	@FXML
	private Label warningLbl;
	
	@FXML
	private Label succText;

    @FXML
    private TextField subjectField;

    @FXML
    private TextField forField;

    @FXML
    private TextArea textArea;

	@FXML
	private GridPane homePane;
	
    private MailManager mm = new MailManager();
    
    @FXML
    void sendMessage(ActionEvent event) {
    	sendBtn.setDisable(true);
    	succText.setVisible(false);
    	warningLbl.setVisible(false);
    	String toEmail = forField.getText();
    	String subject = subjectField.getText();
    	String text = textArea.getText();
    	
    	Task<Boolean> task = new Task<Boolean>() {
	    	
			@Override
			protected Boolean call() throws Exception {
				
		    	if (!toEmail.isEmpty() && (toEmail.contains("ais3") || toEmail.contains("asi3"))) {
		    		if (mm.sendEmail(toEmail, subject, text)) {
		    			return true;
		    		} else {
		    			return false;
		    		}
		    	} else if (!toEmail.isEmpty()) {
		    		if (EmailUtil.getEmailUtil().sendEmail(toEmail, subject, text)) {
		    			mm.sentOut(toEmail,subject,text);
		    			return true;
		    		} else {
		    			return false;
		    		}
		    	}
				return false;
			}
    	};
    	
    	task.setOnFailed(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);	
				warningLbl.setVisible(true);
			}
		});
    	
    	task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				if (task.getValue()) {
					succText.setVisible(true);
	    			forField.clear();	
	    			sendBtn.setDisable(false);
				} else {
					warningLbl.setVisible(true);
					sendBtn.setDisable(false);
				}
			}
		});
    	new Thread(task).start();
    }
}
