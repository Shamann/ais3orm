package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import sk.fiit.vava.core.config.Loggers;
import sk.fiit.vava.core.dataObjects.SubjectList;
import sk.fiit.vava.core.models.ManageTeachers;
import sk.fiit.vava.core.models.SubjectManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class LectorManagementController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		fillSubjects();
		subjectTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SubjectList>() {

			@Override
			public void changed(ObservableValue<? extends SubjectList> observable, SubjectList oldValue, SubjectList newValue) {
					if(subjectTable.getSelectionModel().getSelectedItem()!=null) {
						fillUpdates(subjectTable.getSelectionModel().getSelectedItem());
						succAddLbl.setVisible(false);
						succUpdLbl.setVisible(false);
				}
			}
		});
	}

	private void fillUpdates(final SubjectList selectedItem) {	
		idUpd.setText(selectedItem.getSubjectID() + "");
		nameUpd.setText(selectedItem.getName());
		creditsUpd.setText(selectedItem.getCredits() + "");
		lectorUpd.setText(selectedItem.getLectorName());
		aboutUpd.setText(selectedItem.getText());
		
		if (selectedItem.getLector() == -1) {
			dropUpd.setDisable(true);
			becomeUpd.setDisable(false);
			applyUpd.setDisable(false);
		} else 
		if (selectedItem.getLector() != UserInfo.getUser().getUserID()) {
			dropUpd.setDisable(true);
			becomeUpd.setDisable(true);
			applyUpd.setDisable(true);
			
		} else
		if (selectedItem.getLector() == UserInfo.getUser().getUserID()) {
			dropUpd.setDisable(false);
			becomeUpd.setDisable(true);
			applyUpd.setDisable(false);
			
		}

	}

	@FXML
	private Label succUpdLbl;
	@FXML
	private Label succAddLbl;
	@FXML
	private TextField idUpd;
	@FXML
	private TextField nameUpd;
	@FXML
	private TextField creditsUpd;
	@FXML
	private TextField lectorUpd;
	@FXML
	private TextArea aboutUpd;
	@FXML
	private Button becomeUpd;
	@FXML
	private Button dropUpd;
	@FXML
	private Button applyUpd;
	@FXML
	private Button createBtn;
	@FXML
	private TextField nameAdd;
	@FXML
	private TextField creditsAdd;
	@FXML
	private TextArea aboutAdd;
	@FXML
	private TableView<SubjectList> subjectTable;
	@FXML
	private TableColumn<SubjectList, Integer> idCol;
	@FXML
	private TableColumn<SubjectList, String> nameCol;
	@FXML
	private TableColumn<SubjectList, Integer> creditsCol;
	@FXML
	private TableColumn<SubjectList, String> lectorCol;
	@FXML
	private TableColumn<SubjectList, String> aboutCol;
	
	private ObservableList<SubjectList> subjects = FXCollections.observableArrayList();
	
	private SubjectManager subjectManager = new SubjectManager();
	private ManageTeachers teacherManager = new ManageTeachers();
	
	private void fillSubjects() {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				subjects = subjectManager.getAllSubjects();
				subjectTable.setItems(subjects);
				fillColumns();
				return null;
			}
			
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
			}
			
		});
		new Thread(task).start();
		
	}
	
	private void fillColumns() {
		Task<Void> task = new Task<Void>() {
		
			@Override
			protected Void call() throws Exception {
				idCol.setCellValueFactory(SubjectList.getIDproperty());
				nameCol.setCellValueFactory(SubjectList.getNameProperty());
				creditsCol.setCellValueFactory(SubjectList.getCreditsProperty());
				lectorCol.setCellValueFactory(SubjectList.getLectorProperty());
				aboutCol.setCellValueFactory(SubjectList.getAboutProperty());
				return null;
			}
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
			
		});
		new Thread(task).start();
	}

	@FXML
	private void becomeLector(ActionEvent event) {
		Task<Void> task = new Task<Void>() {
			
			@Override
			protected Void call() throws Exception {
				teacherManager.becomeLector(Integer.valueOf(idUpd.getText()), UserInfo.getUser().getUserID());
				fillSubjects();
			return null;
			}
		};
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent event) {
				succUpdLbl.setVisible(true);
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {
	
			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
			}
		});
		new Thread(task).start();
	}
	
	@FXML
	private void dropLectoring(ActionEvent event) {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				teacherManager.dropLectoring(idUpd.getText());
				fillSubjects();
				return null;
			}
		};
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				succUpdLbl.setVisible(true);
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
				task.getException().printStackTrace();
			}
		});
		new Thread(task).start();
	}

	@FXML
	private void createSubject(ActionEvent event) {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				teacherManager.createNewSubject(nameAdd.getText(), creditsAdd.getText(), aboutAdd.getText(), UserInfo.getUser().getUserID());
				return null;
			}
		};
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				succAddLbl.setVisible(true);
				fillSubjects();
			}
		});
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				task.getException().printStackTrace();
			}
		});
		new Thread(task).start();
	}

	
	@FXML
	private void stashSuccesUpd() {
		succUpdLbl.setVisible(false);
	}
	@FXML
	private void stashsuccessCreate() {
		succAddLbl.setVisible(false);
	}
	
	@FXML
	private void applyUpd() {
		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				subjectManager.updateSubject(idUpd.getText(), nameUpd.getText(), creditsUpd.getText(), aboutUpd.getText());
				return null;
			}
			
		};
		task.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				Loggers.getLoggers().LG_LOGGER.log(Level.WARNING, "Task problems: " + this.getClass().getName() + " from event: " + event.getEventType(), event);
				task.getException().printStackTrace();
			}
		});
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				succUpdLbl.setVisible(true);
				fillSubjects();
			}
		});

	}
	
	@FXML
	private void stashUpd() {
		idUpd.clear();
		nameUpd.clear();
		creditsUpd.clear();
		lectorUpd.clear();
		aboutUpd.clear();
		becomeUpd.setDisable(true);
		dropUpd.setDisable(true);
		applyUpd.setDisable(true);
	}
	
	@FXML
	private void stashAdd(ActionEvent event) {
		nameAdd.clear();
		creditsAdd.clear();
		aboutAdd.clear();
		createBtn.setDisable(true);
	}
}
