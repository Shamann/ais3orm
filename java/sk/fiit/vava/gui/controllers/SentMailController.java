package sk.fiit.vava.gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import sk.fiit.vava.core.dataObjects.MailTable;
import sk.fiit.vava.core.models.MailManager;
import sk.fiit.vava.core.user.UserInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;

public class SentMailController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillMails();
		mailTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MailTable>() {
		
			@Override
			public void changed(ObservableValue<? extends MailTable> observable,MailTable oldValue, MailTable newValue) {
				
				if(mailTable.getSelectionModel().getSelectedItem()!=null) {
					textArray.setText(mailTable.getSelectionModel().getSelectedItem().getText());
				}
			}
		});
	}

	@FXML
    private TableColumn<MailTable, Integer> sizeC;

    @FXML
    private TableColumn<MailTable, String> subjectC;

    @FXML
    private TableColumn<MailTable, String> dateC;

    @FXML
    private TableColumn<MailTable, String> forC;
    
    @FXML
    private TableView<MailTable> mailTable;

    @FXML
    private TextArea textArray;

    @FXML
    private Button deleteBtn;

    private MailManager mm = new MailManager();
    
    @FXML
    void deleteMail(ActionEvent event) {
    	if (mailTable.getSelectionModel().getSelectedItem()!=null) {
	    	mm.deleteMail(mailTable.getSelectionModel().getSelectedItem().getId(),UserInfo.getUser().getUserID());
	    	fillMails();
    	}
    }
    
    private void fillMails() {
    	ObservableList<MailTable> mt = mm.getSentMails(UserInfo.getUser().getUserID(), UserInfo.getUser().getMail());
    	mailTable.setItems(mt);
    	dateC.setCellValueFactory(MailTable.getSentProperty());
    	forC.setCellValueFactory(MailTable.getToProperty());
    	subjectC.setCellValueFactory(MailTable.getSubjectProperty());
		sizeC.setCellValueFactory(MailTable.getSizeProperty());
	}

}
